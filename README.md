# Online Banking System

This is the best banking system for managing accounts of customers, watching current market currencies changes and also calculating loans. Ultimate choice recommended by MUNI :)

Project members:

| name                       | uco    |
|----------------------------|--------|
| Jonáš Novotný              | 485364 |
| Martin Vrzoň               | 514165 |
| Tomáš Martykán (Team lead) | 557315 |

Project properties: Java 21, maven, openapi, swagger, h2...

Run modules: 
```
mvn clean install && mvn spring-boot:run
```

Run using Docker compose:
```
docker-compose up
```

By default the services run on the following HTTP ports:
- 8081: core
- 8082: currency
- 8083: loans
- 8080: OIDC client

### Scopes description

- test_read: all methods
- test_write: post/put/delete methods
- test_1: admin
- test_2: normal user

## Core module

Module for handling bank accounts and customers.

<img src="./documentation/core/UC.jpg" alt="Use case for core"/>
<img src="./documentation/core/ERD.jpg" alt="ERD for core">
<img src="./documentation/core/DTOs.png" alt="Class diagram for DTOs in core">

## Currency module

Module for handling currencies conversions and monitoring changes on market.

<img src="./documentation/currency/UC.jpg" alt="Use case for currency"/>
<img src="./documentation/currency/ERD.jpg" alt="ERD for currency">
<img src="./documentation/currency/DTOs.png" alt="Class diagram for DTOs in currency">

## Loans module

Module for handling loan calculation and contract creation.

<img src="./documentation/loans/UC.jpg" alt="Use case for loans"/>
<img src="./documentation/loans/ERD.jpg" alt="ERD for loans">
<img src="./documentation/loans/DTOs.png" alt="Class diagram for DTOs in loans">

## Observability

Grafana is running on port 3000 and Prometheus on port 9090.
Default credentials for Grafana are `admin:grafana`.

Dashboards are provisioned automatically, the ones used are [Spring Boot Observability](https://grafana.com/grafana/dashboards/17175-spring-boot-observability/) and [JVM (Micrometer)](https://grafana.com/grafana/dashboards/4701-jvm-micrometer/) from Grafana's community library.

## Test Scenario

Test scenario is using apache-jmeter test suite. Firstly, download and unzip Jmeter(https://jmeter.apache.org/download_jmeter.cgi) in test-scenario folder.
The scenario tests seeding database and then running various actions on the system, including money transfers, withdrawals and deposits. 

Run scenario by:
1) starting all modules of the application (or using docker-compose)
2) generating a JWT token using the client module
3) replacing the token in the `TestScenario1.jmx` file, on line 19
4) executing the following command:
```
./test-scenario/apache-jmeter-5.6.3/bin/jmeter -n -t ./test-scenario/TestScenario1.jmx -l ./test-scenario/results.jtl -j ./test-scenario/jmeter.log
```
Results can be found in `test-scenario/results.jtl` file.
