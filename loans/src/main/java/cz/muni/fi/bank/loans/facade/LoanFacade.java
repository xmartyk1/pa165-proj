package cz.muni.fi.bank.loans.facade;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.mapper.LoanMapper;
import cz.muni.fi.bank.loans.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class LoanFacade {
    private final LoanService loanService;

    private final LoanMapper loanMapper;

    @Autowired
    public LoanFacade(LoanService loanService, LoanMapper loanMapper) {
        this.loanService = loanService;
        this.loanMapper = loanMapper;
    }

    @Transactional(readOnly = true)
    public Page<LoanDto> findAll(Pageable pageable) {
        return loanMapper.mapToPageDto(loanService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public LoanDto findById(Long id) {
        return loanMapper.mapToDto(loanService.findById(id));
    }
}
