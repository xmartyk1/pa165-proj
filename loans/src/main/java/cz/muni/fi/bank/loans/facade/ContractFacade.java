package cz.muni.fi.bank.loans.facade;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.mapper.ContractMapper;
import cz.muni.fi.bank.loans.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ContractFacade {
    private final ContractService contractService;

    private final ContractMapper contractMapper;

    @Autowired
    public ContractFacade(ContractService contractService, ContractMapper contractMapper) {
        this.contractService = contractService;
        this.contractMapper = contractMapper;
    }

    @Transactional(readOnly = true)
    public Page<ContractDto> findAll(Pageable pageable) {
        return contractMapper.mapToPageDto(contractService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public ContractDto findById(Long id) {
        return contractMapper.mapToDto(contractService.findById(id));
    }
}
