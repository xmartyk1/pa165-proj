package cz.muni.fi.bank.loans.api;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoanDto {
    @Id
    @GeneratedValue
    private Long id;
    private Long amount;
    private BigDecimal interestRate;
}
