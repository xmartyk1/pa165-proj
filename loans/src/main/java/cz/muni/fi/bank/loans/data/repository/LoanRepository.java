package cz.muni.fi.bank.loans.data.repository;

import cz.muni.fi.bank.loans.data.model.Loan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {

    @Modifying
    @Query("UPDATE Loan l SET l.amount = :#{#updateLoan.amount}, l.interestRate = :#{#updateLoan.interestRate} WHERE l.id = :#{#updateLoan.id}")
    void updateLoan(@Param("updateLoan") Loan updateLoan);
}
