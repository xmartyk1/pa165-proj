package cz.muni.fi.bank.loans.controller;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.facade.LoanFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = LoanController.BASE_PATH)
public class LoanController {

    private final LoanFacade loanFacade;

    public static final String BASE_PATH = "/api/loans";

    @Autowired
    public LoanController(LoanFacade loanFacade) {
        this.loanFacade = loanFacade;
    }

    @GetMapping
    @Operation(summary = "Get all loans",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("hasAuthority('SCOPE_test_1') OR hasAuthority('SCOPE_test_2')")
    @PageableAsQueryParam
    public List<LoanDto> getAll(@Parameter(hidden = true) Pageable pageable) {
        return new ArrayList<>(loanFacade.findAll(pageable).getContent());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get loan by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostAuthorize("hasAuthority('SCOPE_test_1') OR hasAuthority('SCOPE_test_2')")
    public ResponseEntity<LoanDto> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(loanFacade.findById(id));
    }
}
