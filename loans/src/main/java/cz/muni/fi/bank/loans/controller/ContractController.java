package cz.muni.fi.bank.loans.controller;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.facade.ContractFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = ContractController.BASE_PATH)
public class ContractController {

    private final ContractFacade contractFacade;

    public static final String BASE_PATH = "/api/contracts";

    @Autowired
    public ContractController(ContractFacade contractFacade) {
        this.contractFacade = contractFacade;
    }

    @GetMapping
    @Operation(summary = "Get all contracts",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND filterObject.email.equals(authentication.principal.name))")
    @PageableAsQueryParam
    public List<ContractDto> getAll(@Parameter(hidden=true) Pageable pageable) {
        return new ArrayList<>(contractFacade.findAll(pageable).getContent());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get contract by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostAuthorize("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND returnObject.body.email.equals(authentication.principal.name))")
    public ResponseEntity<ContractDto> getById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(contractFacade.findById(id));
    }
}
