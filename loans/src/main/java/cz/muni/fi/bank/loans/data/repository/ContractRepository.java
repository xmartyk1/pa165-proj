package cz.muni.fi.bank.loans.data.repository;

import cz.muni.fi.bank.loans.data.model.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractRepository extends JpaRepository<Contract, Long> {

    @Modifying
    @Query("UPDATE Contract c SET c.name = :#{#updateCustomer.name}, c.surname = :#{#updateCustomer.surname}, c.born = :#{#updateCustomer.born}, c.governmentId = :#{#updateCustomer.governmentId}, c.months = :#{#updateCustomer.months} WHERE c.id = :#{#updateCustomer.id}")
    void updateContract(@Param("updateCustomer") Contract updateCustomer);
}
