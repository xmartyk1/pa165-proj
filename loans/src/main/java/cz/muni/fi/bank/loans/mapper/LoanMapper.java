package cz.muni.fi.bank.loans.mapper;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.data.model.Loan;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = {Loan.class})
public interface LoanMapper {

    LoanDto mapToDto(Loan loan);

    List<LoanDto> mapToList(List<Loan> loans);

    default Page<LoanDto> mapToPageDto(Page<Loan> loans) {
        return new PageImpl<>(mapToList(loans.getContent()), loans.getPageable(), loans.getTotalPages());
    }
}
