package cz.muni.fi.bank.loans.service;

import cz.muni.fi.bank.loans.data.database.DatabaseManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DatabaseService {

    private final DatabaseManager databaseManager;

    @Autowired
    public DatabaseService(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public void seedDatabase() {
        databaseManager.seedDatabase();
    }

    public void clearDatabase() {
        databaseManager.clearDatabase();
    }
}
