package cz.muni.fi.bank.loans.api;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ContractDto {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String email;

    private Long loanId;
    private String name;
    private String surname;
    private LocalDate born;
    private Long governmentId;
    private Long months;
}
