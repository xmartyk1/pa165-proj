package cz.muni.fi.bank.loans.mapper;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.data.model.Contract;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = {Contract.class})
public interface ContractMapper {

    @Mapping(source = "loan.id", target = "loanId")
    ContractDto mapToDto(Contract contract);

    List<ContractDto> mapToList(List<Contract> contracts);

    default Page<ContractDto> mapToPageDto(Page<Contract> contracts) {
        return new PageImpl<>(mapToList(contracts.getContent()), contracts.getPageable(), contracts.getTotalPages());
    }
}
