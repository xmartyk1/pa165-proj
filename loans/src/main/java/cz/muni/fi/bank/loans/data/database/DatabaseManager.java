package cz.muni.fi.bank.loans.data.database;

import cz.muni.fi.bank.loans.data.model.Contract;
import cz.muni.fi.bank.loans.data.model.Loan;
import cz.muni.fi.bank.loans.data.repository.ContractRepository;
import cz.muni.fi.bank.loans.data.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class DatabaseManager {

    private final LoanRepository loanRepository;
    private final ContractRepository contractRepository;

    @Autowired
    public DatabaseManager(LoanRepository loanRepository, ContractRepository contractRepository) {
        this.loanRepository = loanRepository;
        this.contractRepository = contractRepository;
    }

    @Transactional
    public void clearDatabase() {
        contractRepository.deleteAll();
        loanRepository.deleteAll();
    }

    @Transactional
    public void seedDatabase() {

        clearDatabase();

        Loan loan1 = loanRepository.save(new Loan(null, 1000L, BigDecimal.valueOf(0.05)));
        Loan loan2 = loanRepository.save(new Loan(null, 2000L, BigDecimal.valueOf(0.1)));

        contractRepository.save(new Contract(null, loan1,"john.doe@gmail.com", "John", "Doe", LocalDate.of(1990, 1, 1), 123L, 12L));
        contractRepository.save(new Contract(null, loan2, "jane.doe@outlook.com", "Jane", "Doe", LocalDate.of(1990, 12, 12),  456L, 24L));
    }
}
