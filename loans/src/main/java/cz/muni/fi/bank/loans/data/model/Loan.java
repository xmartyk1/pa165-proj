package cz.muni.fi.bank.loans.data.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@Table(name = "loan")
public class Loan implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private Long amount;
    private BigDecimal interestRate;
}
