package cz.muni.fi.bank.loans.e2e;

import cz.muni.fi.bank.loans.controller.LoanController;
import cz.muni.fi.bank.loans.data.model.Loan;
import cz.muni.fi.bank.loans.data.repository.LoanRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class LoansIT {

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllLoans_callGetLoans_getEmptyLoans() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(LoanController.BASE_PATH, 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
    }

    @Test
    void getAllLoans_callGetLoans_getLoans() throws Exception {
        loanRepository.deleteAll();
        Loan loan = Loan.builder()
                .amount(100_000L)
                .interestRate(new BigDecimal("69.69"))
                .build();
        loanRepository.save(loan);
        loan = loanRepository.findAll().getFirst();

        mockMvc.perform(MockMvcRequestBuilders
                        .get(LoanController.BASE_PATH + "/" + loan.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));
    }
}
