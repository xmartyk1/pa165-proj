package cz.muni.fi.bank.loans.repository;

import cz.muni.fi.bank.loans.data.model.Loan;
import cz.muni.fi.bank.loans.data.repository.LoanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DataJpaTest
public class LoanRepositoryTest {

    @Autowired
    private LoanRepository loanRepository;

    @BeforeEach
    void initData() {

        loanRepository.deleteAll();
        Loan loan = Loan.builder()
                .amount(100_000L)
                .interestRate(new BigDecimal("0.069"))
                .build();
        loanRepository.save(loan);
    }

    @Test
    void findById_findLoan_loanReturned() {
        assertFalse(loanRepository.findAll().isEmpty());
        Loan loan = loanRepository.findAll().getFirst();
        assertEquals(100_000L, loan.getAmount());
        assertEquals(new BigDecimal("0.069"), loan.getInterestRate());
    }

    @Test
    void updateLoan_loanUpdated_loanChanged() {
        // Arrange
        Long newAmount = 42L;
        BigDecimal newInterestRate = new BigDecimal(42);
        assertFalse(loanRepository.findAll().isEmpty());
        Loan loan = loanRepository.findAll().getFirst();
        loan.setAmount(newAmount);
        loan.setInterestRate(newInterestRate);

        // Act
        loanRepository.updateLoan(loan);
        Loan updatedLoan = loanRepository.findAll().getFirst();

        // Assert
        assertEquals(updatedLoan.getAmount(), newAmount);
        assertEquals(updatedLoan.getInterestRate(), newInterestRate);
    }
}
