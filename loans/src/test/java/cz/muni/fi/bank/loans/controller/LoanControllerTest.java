package cz.muni.fi.bank.loans.controller;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.facade.LoanFacade;
import cz.muni.fi.bank.loans.util.TestLoanDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class LoanControllerTest {
    @Mock
    private LoanFacade loanFacade;

    @InjectMocks
    private LoanController loanController;

    @Test
    void getById_loanFound_returnsLoan() {
        // Arrange
        Mockito.when(loanFacade.findById(1L)).thenReturn(TestLoanDataFactory.loanDto);

        // Act
        ResponseEntity<LoanDto> foundEntity = loanController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(foundEntity.getBody()).getId()).isEqualTo(TestLoanDataFactory.loanDto.getId());
    }

    @Test
    void getAll_loansFound_returnsLoans() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(loanFacade.findAll(pageable)).thenReturn(TestLoanDataFactory.loanDtoPage);

        // Act
        List<LoanDto> foundEntity = loanController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestLoanDataFactory.loanDtoPage.getContent());
        Mockito.verify(loanFacade, Mockito.times(1)).findAll(pageable);
    }
}
