package cz.muni.fi.bank.loans.repository;

import cz.muni.fi.bank.loans.data.model.Contract;
import cz.muni.fi.bank.loans.data.model.Loan;
import cz.muni.fi.bank.loans.data.repository.ContractRepository;
import cz.muni.fi.bank.loans.data.repository.LoanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ContractRepositoryTest {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private LoanRepository loanRepository;

    @BeforeEach
    void initData() {

        loanRepository.deleteAll();
        Loan loan = Loan.builder()
                .amount(100_000L)
                .interestRate(new BigDecimal("0.069"))
                .build();
        loanRepository.save(loan);

        contractRepository.deleteAll();
        Contract contract = Contract.builder()
                .loan(loan)
                .name("Jan")
                .surname("Novák")
                .born(LocalDate.of(1970, 1, 1))
                .governmentId(1234567890L)
                .months(12L)
                .email("123456@muni.cz")
                .build();
        contractRepository.save(contract);
    }

    @Test
    void findById_contractFound_contractReturned() {
        assertFalse(contractRepository.findAll().isEmpty());
        Contract contract = contractRepository.findAll().getFirst();
        assertEquals(contract.getName(), "Jan");
        assertEquals(contract.getSurname(), "Novák");
        assertEquals(contract.getGovernmentId(), 1234567890L);
        assertEquals(contract.getMonths(), 12);
        assertEquals(contract.getBorn(), LocalDate.of(1970, 1, 1));
        Loan loan = contract.getLoan();
        assertTrue(loanRepository.findById(loan.getId()).isPresent());
        assertEquals(loan.getId(), loanRepository.findById(loan.getId()).get().getId());
    }

    @Test
    void updateContract_contractUpdated_contractChanged() {
        String newName = "Pepe";
        String newSurname = "The Frog";
        assertFalse(contractRepository.findAll().isEmpty());
        Contract contract = contractRepository.findAll().getFirst();
        contract.setName(newName);
        contract.setSurname(newSurname);

        // Act
        contractRepository.updateContract(contract);
        Contract updatedContract = contractRepository.findAll().getFirst();

        // Assert
        assertEquals(updatedContract.getName(), newName);
        assertEquals(updatedContract.getSurname(), newSurname);
    }
}
