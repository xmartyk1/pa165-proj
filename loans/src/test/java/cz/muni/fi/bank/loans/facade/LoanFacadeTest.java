package cz.muni.fi.bank.loans.facade;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.mapper.LoanMapper;
import cz.muni.fi.bank.loans.service.LoanService;
import cz.muni.fi.bank.loans.util.TestLoanDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class LoanFacadeTest {
    @Mock
    private LoanService loanService;

    @Mock
    private LoanMapper loanMapper;

    @InjectMocks
    private LoanFacade loanFacade;

    @Test
    public void findAll_loansFound_returnsLoans() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(loanService.findAll(pageable)).thenReturn(TestLoanDataFactory.loanPage);
        Mockito.when(loanMapper.mapToPageDto(any())).thenReturn(TestLoanDataFactory.loanDtoPage);

        // Act
        Page<LoanDto> foundEntity = loanFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestLoanDataFactory.loanDtoPage);
    }

    @Test
    public void findById_loanFound_returnsLoan() {
        // Arrange
        Mockito.when(loanService.findById(1L)).thenReturn(TestLoanDataFactory.loan);
        Mockito.when(loanMapper.mapToDto(any())).thenReturn(TestLoanDataFactory.loanDto);

        // Act
        LoanDto foundEntity = loanFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestLoanDataFactory.loanDto);
    }
}
