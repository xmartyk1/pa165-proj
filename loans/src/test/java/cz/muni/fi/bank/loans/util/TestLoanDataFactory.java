package cz.muni.fi.bank.loans.util;

import cz.muni.fi.bank.loans.api.LoanDto;
import cz.muni.fi.bank.loans.data.model.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class TestLoanDataFactory {
    public static Loan loan = getLoanFactory();

    public static LoanDto loanDto = getLoanDtoFactory();

    public static Page<Loan> loanPage = getLoanPageFactory();

    public static Page<LoanDto> loanDtoPage = getLoanDtoPageFactory();

    private static Loan getLoanFactory() {
        return new Loan(1L, 10_000L, new BigDecimal("0.069"));
    }

    private static LoanDto getLoanDtoFactory() {
        return new LoanDto(1L, 10_000L, new BigDecimal("0.069"));
    }

    private static Page<Loan> getLoanPageFactory() {
        return new PageImpl<>(List.of(TestLoanDataFactory.loan));
    }

    private static Page<LoanDto> getLoanDtoPageFactory() {
        return new PageImpl<>(List.of(TestLoanDataFactory.loanDto));
    }
}
