package cz.muni.fi.bank.loans.controller;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.facade.ContractFacade;
import cz.muni.fi.bank.loans.util.TestContractDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ContractControllerTest {
    @Mock
    private ContractFacade contractFacade;

    @InjectMocks
    private ContractController contractController;

    @Test
    void getById_contractFound_returnsContract() {
        // Arrange
        Mockito.when(contractFacade.findById(1L)).thenReturn(TestContractDataFactory.contractDto);

        // Act
        ResponseEntity<ContractDto> foundEntity = contractController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(foundEntity.getBody()).getId()).isEqualTo(TestContractDataFactory.contractDto.getId());
    }

    @Test
    void getAll_contractsFound_returnsContracts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(contractFacade.findAll(pageable)).thenReturn(TestContractDataFactory.contractDtoPage);

        // Act
        List<ContractDto> foundEntity = contractController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestContractDataFactory.contractDtoPage.getContent());
        Mockito.verify(contractFacade, Mockito.times(1)).findAll(pageable);
    }
}
