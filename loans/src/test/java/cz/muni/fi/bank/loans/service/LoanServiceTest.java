package cz.muni.fi.bank.loans.service;

import cz.muni.fi.bank.loans.data.model.Loan;
import cz.muni.fi.bank.loans.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.loans.data.repository.LoanRepository;
import cz.muni.fi.bank.loans.util.TestLoanDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class LoanServiceTest {
    @Mock
    private LoanRepository loanRepository;

    @InjectMocks
    private LoanService loanService;

    @Test
    public void findAll_loansFound_returnsLoans() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(loanRepository.findAll(pageable)).thenReturn(TestLoanDataFactory.loanPage);

        // Act
        Page<Loan> foundEntity = loanService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestLoanDataFactory.loanPage);
    }

    @Test
    public void findById_loanFound_returnsLoan() {
        // Arrange
        Mockito.when(loanRepository.findById(1L)).thenReturn(Optional.ofNullable(TestLoanDataFactory.loan));

        // Act
        Loan foundEntity = loanService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestLoanDataFactory.loan);
    }

    @Test
    void findById_loanNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(loanRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> loanService.findById(1L));
    }
}
