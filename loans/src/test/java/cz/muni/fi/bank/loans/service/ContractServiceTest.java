package cz.muni.fi.bank.loans.service;

import cz.muni.fi.bank.loans.data.model.Contract;
import cz.muni.fi.bank.loans.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.loans.data.repository.ContractRepository;
import cz.muni.fi.bank.loans.util.TestContractDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ContractServiceTest {
    @Mock
    private ContractRepository contractRepository;

    @InjectMocks
    private ContractService contractService;

    @Test
    public void findAll_contractsFound_returnsContracts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(contractRepository.findAll(pageable)).thenReturn(TestContractDataFactory.contractPage);

        // Act
        Page<Contract> foundEntity = contractService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestContractDataFactory.contractPage);
    }

    @Test
    public void findById_contractFound_returnsContract() {
        // Arrange
        Mockito.when(contractRepository.findById(1L)).thenReturn(Optional.ofNullable(TestContractDataFactory.contract));

        // Act
        Contract foundEntity = contractService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestContractDataFactory.contract);
    }

    @Test
    void findById_loanNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(contractRepository.findById(1L)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> contractService.findById(1L));
    }
}
