package cz.muni.fi.bank.loans.util;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.data.model.Contract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class TestContractDataFactory {
    public static Contract contract = getContractFactory();

    public static ContractDto contractDto = getContractDtoFactory();

    public static Page<Contract> contractPage = getContractPageFactory();

    public static Page<ContractDto> contractDtoPage = getContractDtoPageFactory();

    private static Contract getContractFactory() {
        return new Contract(
                1L,
                TestLoanDataFactory.loan,
                "123456@muni.cz",
                "Jan",
                "Novák",
                LocalDate.of(1970, 1, 1),
                1234567890L,
                12L
        );
    }

    private static ContractDto getContractDtoFactory() {
        return new ContractDto(
                1L,
                "213456@muni.cz",
                1L,
                "Jan",
                "Novák",
                LocalDate.of(1970, 1, 1),
                1234567890L,
                12L
        );
    }

    private static Page<Contract> getContractPageFactory() {
        return new PageImpl<>(List.of(TestContractDataFactory.contract));
    }

    private static Page<ContractDto> getContractDtoPageFactory() {
        return new PageImpl<>(List.of(TestContractDataFactory.contractDto));
    }
}
