package cz.muni.fi.bank.loans.facade;

import cz.muni.fi.bank.loans.api.ContractDto;
import cz.muni.fi.bank.loans.mapper.ContractMapper;
import cz.muni.fi.bank.loans.service.ContractService;
import cz.muni.fi.bank.loans.util.TestContractDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ContractFacadeTest {
    @Mock
    private ContractService contractService;

    @Mock
    private ContractMapper contractMapper;

    @InjectMocks
    private ContractFacade contractFacade;

    @Test
    public void findAll_contractsFound_returnsContracts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(contractService.findAll(pageable)).thenReturn(TestContractDataFactory.contractPage);
        Mockito.when(contractMapper.mapToPageDto(any())).thenReturn(TestContractDataFactory.contractDtoPage);

        // Act
        Page<ContractDto> foundEntity = contractFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestContractDataFactory.contractDtoPage);
    }

    @Test
    public void findById_contractFound_returnsContract() {
        // Arrange
        Mockito.when(contractService.findById(1L)).thenReturn(TestContractDataFactory.contract);
        Mockito.when(contractMapper.mapToDto(any())).thenReturn(TestContractDataFactory.contractDto);

        // Act
        ContractDto foundEntity = contractFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestContractDataFactory.contractDto);
    }
}
