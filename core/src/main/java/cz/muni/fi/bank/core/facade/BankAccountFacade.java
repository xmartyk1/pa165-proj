package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.BankAccountDetailedDto;
import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import cz.muni.fi.bank.core.api.post.BankAccountCreateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.mapper.BankAccountMapper;
import cz.muni.fi.bank.core.mapper.ScheduledPaymentMapper;
import cz.muni.fi.bank.core.mapper.TransactionMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.CustomerService;
import cz.muni.fi.bank.core.service.ScheduledPaymentService;
import cz.muni.fi.bank.core.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class BankAccountFacade {

    private final BankAccountMapper bankAccountMapper;
    private final BankAccountService bankAccountService;
    private final CustomerService customerService;
    private final TransactionMapper transactionMapper;
    private final TransactionService transactionService;
    private final ScheduledPaymentMapper scheduledPaymentMapper;
    private final ScheduledPaymentService scheduledPaymentService;

    @Autowired
    public BankAccountFacade(BankAccountService bankAccountService, BankAccountMapper bankAccountMapper, TransactionService transactionService, ScheduledPaymentService scheduledPaymentService, CustomerService customerService, TransactionMapper transactionMapper, ScheduledPaymentMapper scheduledPaymentMapper) {
        this.bankAccountService = bankAccountService;
        this.bankAccountMapper = bankAccountMapper;
        this.transactionService = transactionService;
        this.scheduledPaymentService = scheduledPaymentService;
        this.customerService = customerService;
        this.transactionMapper = transactionMapper;
        this.scheduledPaymentMapper = scheduledPaymentMapper;
    }

    @Transactional(readOnly = true)
    public Page<BankAccountDto> findAll(Pageable pageable) {
        return bankAccountMapper.mapToPageDto(bankAccountService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public BankAccountDetailedDto findById(Long id) {
        List<Transaction> transactions = transactionService.getTransactionsByBankAccountId(id);
        BigDecimal balance = new BigDecimal(0);
        for (Transaction transaction : transactions) {
            balance = balance.add(transaction.getDelta());
        }
        return bankAccountMapper.mapToDetailedDto(
                bankAccountService.findById(id),
                balance,
                transactionMapper.mapToList(transactions),
                scheduledPaymentMapper.mapToList(scheduledPaymentService.getScheduledPaymentsByBankAccountId(id)));
    }

    public BankAccountDto create(BankAccountCreateDto bankAccountCreateDto) {
        Customer customer = customerService.findById(bankAccountCreateDto.getCustomerId());
        return bankAccountMapper.mapToDto(bankAccountService.create(bankAccountCreateDto, customer));
    }

    public BankAccountDto updateById(Long id, BankAccountUpdateDto bankAccountUpdateDto) {
        Customer customerUpdate = bankAccountUpdateDto.getCustomerId() != null ? customerService.findById(bankAccountUpdateDto.getCustomerId()) : null;
        return bankAccountMapper.mapToDto(bankAccountService.updateById(id, bankAccountUpdateDto, customerUpdate));
    }

    public BankAccountDto deleteById(Long id) {
        return bankAccountMapper.mapToDto(bankAccountService.deleteById(id));
    }

    public TransactionDto transfer(Long id, TransactionCreateDto transactionCreateDto) {
        BankAccount fromBankAccount = bankAccountService.findById(id);
        BankAccount toBankAccount = bankAccountService.findById(transactionCreateDto.getBankAccountId());
        return transactionMapper.mapToDto(transactionService.transfer(fromBankAccount, toBankAccount, transactionCreateDto.getDelta()));
    }
}
