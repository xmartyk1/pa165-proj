package cz.muni.fi.bank.core.data.model;

import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import cz.muni.fi.bank.core.data.enums.TransactionType;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
public class Transaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccount bankAccount;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType type;
    @Column(nullable = false)
    private BigDecimal delta;
    @Column(nullable = false)
    @CreationTimestamp
    private Instant timestamp;

    public void update(TransactionUpdateDto transactionCreateDto, BankAccount bankAccountUpdate) {
        bankAccount = bankAccountUpdate != null ? bankAccountUpdate : bankAccount;
        type = transactionCreateDto.getType() != null ? TransactionType.valueOf(transactionCreateDto.getType()) : type;
        delta = transactionCreateDto.getDelta() != null ? transactionCreateDto.getDelta() : delta;
    }
}
