package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScheduledPaymentRepository extends JpaRepository<ScheduledPayment, Long> {

    List<ScheduledPayment> getScheduledPaymentsByBankAccountId(Long accountId);
}
