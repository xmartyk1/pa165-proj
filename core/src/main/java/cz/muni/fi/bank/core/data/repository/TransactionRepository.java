package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    List<Transaction> getTransactionsByBankAccountId(Long bankAccountId);
}
