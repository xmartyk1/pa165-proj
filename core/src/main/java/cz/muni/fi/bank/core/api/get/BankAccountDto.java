package cz.muni.fi.bank.core.api.get;

import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class BankAccountDto extends BankAccountUpdateDto {

    private Long id;
}
