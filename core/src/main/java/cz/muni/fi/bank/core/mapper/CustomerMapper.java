package cz.muni.fi.bank.core.mapper;

import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.data.model.Customer;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring", uses = {Customer.class})
public interface CustomerMapper {

    CustomerDto mapToDto(Customer customer);

    CustomerDetailedDto mapToDetailedDto(Customer customer, List<BankAccountDto> bankAccounts);

    List<CustomerDto> mapToList(List<Customer> customers);

    default Page<CustomerDto> mapToPageDto(Page<Customer> customers) {
        return new PageImpl<>(mapToList(customers.getContent()), customers.getPageable(), customers.getTotalPages());
    }
}
