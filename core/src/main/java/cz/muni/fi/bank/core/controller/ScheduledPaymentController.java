package cz.muni.fi.bank.core.controller;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import cz.muni.fi.bank.core.api.post.ScheduledPaymentCreateDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.facade.ScheduledPaymentFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = ScheduledPaymentController.BASE_PATH)
public class ScheduledPaymentController {

    private final ScheduledPaymentFacade scheduledPaymentFacade;

    public static final String BASE_PATH = "/api/scheduled_payments";

    @Autowired
    public ScheduledPaymentController(ScheduledPaymentFacade scheduledPaymentFacade) {
        this.scheduledPaymentFacade = scheduledPaymentFacade;
    }

    @GetMapping()
    @Operation(summary = "Get all payments",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(
                        @bankAccountFacade.findById(
                            filterObject.bankAccountId
                        ).customerId
                    ).email.equals(authentication.principal.name)
                )
            """)
    @PageableAsQueryParam
    public List<ScheduledPaymentDto> getAll(@Parameter(hidden = true) Pageable pageable) {
        return new ArrayList<>(scheduledPaymentFacade.findAll(pageable).getContent());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get payment by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(
                        @bankAccountFacade.findById(
                            @scheduledPaymentFacade.findById(#id).bankAccountId
                        ).customerId
                    ).email.equals(authentication.principal.name)
                )
            """)
    public ResponseEntity<ScheduledPaymentDto> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(scheduledPaymentFacade.findById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create payment",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(
                                @bankAccountFacade.findById(
                                    #scheduledPaymentCreateDto.bankAccountId
                                ).customerId
                            ).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<ScheduledPaymentDto> create(@Valid @RequestBody ScheduledPaymentCreateDto scheduledPaymentCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(scheduledPaymentFacade.create(scheduledPaymentCreateDto));
    }

    @PatchMapping(path = "/{id}")
    @Operation(summary = "Update payment by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(
                                @bankAccountFacade.findById(
                                    @scheduledPaymentFacade.findById(#id).bankAccountId
                                ).customerId
                            ).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<ScheduledPaymentDto> updateById(@PathVariable("id") Long id, @Valid @RequestBody ScheduledPaymentUpdateDto scheduledPaymentUpdateDto) {
        try {
            return ResponseEntity.ok(scheduledPaymentFacade.updateById(id, scheduledPaymentUpdateDto));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete payment by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1')")
    public ResponseEntity<ScheduledPaymentDto> deleteById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(scheduledPaymentFacade.deleteById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
