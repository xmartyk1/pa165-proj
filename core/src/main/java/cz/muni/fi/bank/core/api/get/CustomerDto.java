package cz.muni.fi.bank.core.api.get;

import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto extends CustomerUpdateDto {

    private Long id;
    private String email;
}
