package cz.muni.fi.bank.core.api.patch;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class BankAccountUpdateDto {

    @Min(1)
    @Max(Long.MAX_VALUE)
    private Long customerId;
    @Size(min = 3, max = 3)
    private String currency;
}
