package cz.muni.fi.bank.core.controller;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.facade.TransactionFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(path = TransactionController.BASE_PATH)
public class TransactionController {

    private final TransactionFacade transactionFacade;

    public static final String BASE_PATH = "/api/transactions";

    @Autowired
    public TransactionController(TransactionFacade transactionFacade) {
        this.transactionFacade = transactionFacade;
    }

    @GetMapping()
    @Operation(summary = "Get all transactions",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(
                        @bankAccountFacade.findById(
                            filterObject.bankAccountId
                        ).customerId
                    ).email.equals(authentication.principal.name)
                )
            """)
    @PageableAsQueryParam
    public List<TransactionDto> getAll(@Parameter(hidden = true) Pageable pageable) {
        return new ArrayList<>(transactionFacade.findAll(pageable).getContent());
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get transaction by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(
                        @bankAccountFacade.findById(
                            @transactionFacade.findById(#id).bankAccountId
                        ).customerId
                    ).email.equals(authentication.principal.name)
                )
            """)
    public ResponseEntity<TransactionDto> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(transactionFacade.findById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create transaction",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(
                                @bankAccountFacade.findById(
                                    #transactionCreateDto.bankAccountId
                                ).customerId
                            ).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<TransactionDto> create(@Valid @RequestBody TransactionCreateDto transactionCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(transactionFacade.create(transactionCreateDto));
    }

    @PatchMapping(path = "/{id}")
    @Operation(summary = "Update transaction by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(
                                @bankAccountFacade.findById(
                                    @transactionFacade.findById(#id).bankAccountId
                                ).customerId
                            ).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<TransactionDto> updateById(@PathVariable("id") Long id, @Valid @RequestBody TransactionUpdateDto transactionUpdateDto) {
        try {
            return ResponseEntity.ok(transactionFacade.updateById(id, transactionUpdateDto));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete transaction by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1')")
    public ResponseEntity<TransactionDto> deleteById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(transactionFacade.deleteById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
