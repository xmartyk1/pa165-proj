package cz.muni.fi.bank.core.controller;

import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import cz.muni.fi.bank.core.api.post.BankAccountCreateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.facade.BankAccountFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = BankAccountController.BASE_PATH)
public class BankAccountController {

    private final BankAccountFacade bankAccountFacade;

    public static final String BASE_PATH = "/api/bank_accounts";

    @Autowired
    public BankAccountController(BankAccountFacade bankAccountFacade) {
        this.bankAccountFacade = bankAccountFacade;
    }

    @GetMapping
    @Operation(summary = "Get all accounts",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(filterObject.customerId).email.equals(authentication.principal.name)
                )
            """)
    @PageableAsQueryParam
    public List<BankAccountDto> getAll(@Parameter(hidden=true) Pageable pageable) {
        return new ArrayList<>(bankAccountFacade.findAll(pageable).getContent());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get account by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(@bankAccountFacade.findById(#id).customerId).email.equals(authentication.principal.name)
                )
            """)
    public ResponseEntity<BankAccountDto> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(bankAccountFacade.findById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/{id}/transfer")
    @Operation(summary = "Transfer for account by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                hasAuthority('SCOPE_test_1') OR (
                    hasAuthority('SCOPE_test_2') AND
                    @customerFacade.findById(@bankAccountFacade.findById(#id).customerId).email.equals(authentication.principal.name)
                )
            """)
    public ResponseEntity<TransactionDto> transfer(@PathVariable("id") Long id, @Valid @RequestBody TransactionCreateDto transactionCreateDto) {
        try {
            return ResponseEntity.ok(bankAccountFacade.transfer(id, transactionCreateDto));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create account",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(#bankAccountCreateDto.customerId).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<BankAccountDto> create(@Valid @RequestBody BankAccountCreateDto bankAccountCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bankAccountFacade.create(bankAccountCreateDto));
    }

    @PatchMapping(path = "/{id}")
    @Operation(summary = "Update account by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("""
                        hasAuthority('SCOPE_test_1') OR (
                            hasAuthority('SCOPE_test_2') AND
                            @customerFacade.findById(@bankAccountFacade.findById(#id).customerId).email.equals(authentication.principal.name)
                        )
            """)
    public ResponseEntity<BankAccountDto> updateById(@PathVariable("id") Long id, @Valid @RequestBody BankAccountUpdateDto bankAccountUpdateDto) {
        try {
            return ResponseEntity.ok(bankAccountFacade.updateById(id, bankAccountUpdateDto));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete account by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1')")
    public ResponseEntity<BankAccountDto> deleteById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(bankAccountFacade.deleteById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
