package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import cz.muni.fi.bank.core.api.post.CustomerCreateDto;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.repository.CustomerRepository;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Transactional(readOnly = true)
    public Page<Customer> findAll(Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Customer findById(Long id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Customer with id: " + id + " was not found."));
    }

    public Customer create(CustomerCreateDto createCustomer) {
        Customer customer = new Customer();
        customer.setName(createCustomer.getName());
        customer.setSurname(createCustomer.getSurname());
        customer.setBirthDate(createCustomer.getBirthDate());
        customer.setEmail(createCustomer.getEmail());
        return customerRepository.save(customer);
    }

    public Customer updateById(Long id, CustomerUpdateDto updateCustomer) {
        Customer customer = findById(id);
        customer.update(updateCustomer);
        try {
            customerRepository.save(customer);
            return customer;
        } catch (Exception e) {
            throw new ResourceNotFoundException("Customer with id: " + id + " was not found.");
        }
    }

    public Customer deleteById(Long id) {
        Customer customer = findById(id);
        try {
            customerRepository.delete(customer);
            return customer;
        } catch (Exception e) {
            throw new ResourceNotFoundException("Customer with id: " + id + " was not found.");
        }
    }
}
