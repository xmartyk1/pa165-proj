package cz.muni.fi.bank.core.api.post;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BankAccountCreateDto {

    @NotNull
    @Min(1)
    @Max(Long.MAX_VALUE)
    private Long customerId;
    @NotNull
    @Size(min = 3, max = 3)
    private String currency;
}
