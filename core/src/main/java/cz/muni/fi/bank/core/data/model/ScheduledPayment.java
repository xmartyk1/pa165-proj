package cz.muni.fi.bank.core.data.model;

import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
public class ScheduledPayment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "bank_account_id", nullable = false)
    private BankAccount bankAccount;
    @Column(nullable = false)
    private LocalDate dueDate;
    @Column(nullable = false)
    private BigDecimal amount;
    @Column(nullable = false)
    private Integer frequencyInDays;

    public void update(ScheduledPaymentUpdateDto scheduledPaymentUpdateDto, BankAccount bankAccountUpdate) {
        bankAccount = bankAccountUpdate != null ? bankAccountUpdate : bankAccount;
        dueDate = scheduledPaymentUpdateDto.getDueDate() != null ? scheduledPaymentUpdateDto.getDueDate() : dueDate;
        amount = scheduledPaymentUpdateDto.getAmount() != null ? scheduledPaymentUpdateDto.getAmount() : amount;
        frequencyInDays = scheduledPaymentUpdateDto.getFrequencyInDays() != null ? scheduledPaymentUpdateDto.getFrequencyInDays() : frequencyInDays;
    }
}
