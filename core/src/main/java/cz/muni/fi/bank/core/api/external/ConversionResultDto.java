package cz.muni.fi.bank.core.api.external;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
public class ConversionResultDto {
    private String baseCurrency;
    private String targetCurrency;
    private BigDecimal amount;
    private BigDecimal resultAmount;

    public String getBaseCurrency() {
        return this.baseCurrency;
    }

    public String getTargetCurrency() {
        return this.targetCurrency;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public BigDecimal getResultAmount() {
        return this.resultAmount;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setResultAmount(BigDecimal resultAmount) {
        this.resultAmount = resultAmount;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ConversionResultDto)) return false;
        final ConversionResultDto other = (ConversionResultDto) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$baseCurrency = this.getBaseCurrency();
        final Object other$baseCurrency = other.getBaseCurrency();
        if (this$baseCurrency == null ? other$baseCurrency != null : !this$baseCurrency.equals(other$baseCurrency))
            return false;
        final Object this$targetCurrency = this.getTargetCurrency();
        final Object other$targetCurrency = other.getTargetCurrency();
        if (this$targetCurrency == null ? other$targetCurrency != null : !this$targetCurrency.equals(other$targetCurrency))
            return false;
        final Object this$amount = this.getAmount();
        final Object other$amount = other.getAmount();
        if (this$amount == null ? other$amount != null : !this$amount.equals(other$amount)) return false;
        final Object this$resultAmount = this.getResultAmount();
        final Object other$resultAmount = other.getResultAmount();
        if (this$resultAmount == null ? other$resultAmount != null : !this$resultAmount.equals(other$resultAmount))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ConversionResultDto;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $baseCurrency = this.getBaseCurrency();
        result = result * PRIME + ($baseCurrency == null ? 43 : $baseCurrency.hashCode());
        final Object $targetCurrency = this.getTargetCurrency();
        result = result * PRIME + ($targetCurrency == null ? 43 : $targetCurrency.hashCode());
        final Object $amount = this.getAmount();
        result = result * PRIME + ($amount == null ? 43 : $amount.hashCode());
        final Object $resultAmount = this.getResultAmount();
        result = result * PRIME + ($resultAmount == null ? 43 : $resultAmount.hashCode());
        return result;
    }

    public String toString() {
        return "ConversionResultDto(baseCurrency=" + this.getBaseCurrency() + ", targetCurrency=" + this.getTargetCurrency() + ", amount=" + this.getAmount() + ", resultAmount=" + this.getResultAmount() + ")";
    }
}
