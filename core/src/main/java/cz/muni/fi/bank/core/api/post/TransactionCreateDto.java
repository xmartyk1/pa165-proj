package cz.muni.fi.bank.core.api.post;

import cz.muni.fi.bank.core.data.enums.TransactionType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class TransactionCreateDto {

    @NotNull
    @Min(1)
    @Max(Long.MAX_VALUE)
    private Long bankAccountId;
    @Pattern(regexp = TransactionType.REGEX)
    private String type;
    @NotNull
    private BigDecimal delta;
}
