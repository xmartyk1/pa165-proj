package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {

    @Query("SELECT bankAccount FROM BankAccount bankAccount WHERE bankAccount.customer=:customer")
    List<BankAccount> getBankAccountsByCustomer(Customer customer);
}
