package cz.muni.fi.bank.core.api.patch;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class CustomerUpdateDto {

    @Size(min = 2, max = 50)
    private String name;
    @Size(min = 2, max = 50)
    private String surname;
    @Past
    private LocalDate birthDate;
    @Email
    private String email;
}
