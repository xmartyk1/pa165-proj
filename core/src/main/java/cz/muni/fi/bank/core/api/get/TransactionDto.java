package cz.muni.fi.bank.core.api.get;

import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class TransactionDto extends TransactionUpdateDto {

    private Long id;
    private Instant timestamp;
}
