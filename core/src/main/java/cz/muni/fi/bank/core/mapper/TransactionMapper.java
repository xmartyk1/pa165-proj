package cz.muni.fi.bank.core.mapper;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.data.model.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    @Mapping(source = "transaction.bankAccount.id", target = "bankAccountId")
    TransactionDto mapToDto(Transaction transaction);

    List<TransactionDto> mapToList(List<Transaction> transactions);

    default Page<TransactionDto> mapToPageDto(Page<Transaction> transactions) {
        return new PageImpl<>(mapToList(transactions.getContent()), transactions.getPageable(), transactions.getTotalPages());
    }
}
