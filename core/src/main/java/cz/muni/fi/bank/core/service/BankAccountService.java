package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import cz.muni.fi.bank.core.api.post.BankAccountCreateDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.repository.BankAccountRepository;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BankAccountService {

    private final BankAccountRepository bankAccountRepository;

    @Autowired
    public BankAccountService(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    @Transactional(readOnly = true)
    public Page<BankAccount> findAll(Pageable pageable) {
        return bankAccountRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public BankAccount findById(Long id) {
        return bankAccountRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("BankAccount with id: " + id + " was not found."));
    }

    public List<BankAccount> getBankAccountsByCustomer(Customer customer) {
        try {
            return bankAccountRepository.getBankAccountsByCustomer(customer);
        } catch (Exception e) {
            throw new ResourceNotFoundException("BankAccount with customerId: " + customer.getId() + " was not found.");
        }
    }

    public BankAccount create(BankAccountCreateDto bankAccountCreateDto, Customer customer) {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setCustomer(customer);
        bankAccount.setCurrency(bankAccountCreateDto.getCurrency());
        return bankAccountRepository.save(bankAccount);
    }

    public BankAccount updateById(Long id, BankAccountUpdateDto updateBankAccount, Customer customer) {
        BankAccount bankAccount = findById(id);
        bankAccount.update(updateBankAccount, customer);
        try {
            bankAccountRepository.save(bankAccount);
            return bankAccount;
        } catch (Exception e) {
            throw new ResourceNotFoundException("BankAccount with id: " + id + " was not found.");
        }
    }

    public BankAccount deleteById(Long id) {
        BankAccount bankAccount = findById(id);
        try {
            bankAccountRepository.delete(bankAccount);
            return bankAccount;
        } catch (Exception e) {
            throw new ResourceNotFoundException("BankAccount with id: " + id + " was not found.");
        }
    }
}
