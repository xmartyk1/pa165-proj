package cz.muni.fi.bank.core.mapper;

import cz.muni.fi.bank.core.api.get.BankAccountDetailedDto;
import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.math.BigDecimal;
import java.util.List;

@Mapper(componentModel = "spring")
public interface BankAccountMapper {

    @Mapping(source = "bankAccount.customer.id", target = "customerId")
    BankAccountDto mapToDto(BankAccount bankAccount);

    @Mapping(source = "bankAccount.customer.id", target = "customerId")
    BankAccountDetailedDto mapToDetailedDto(BankAccount bankAccount, BigDecimal balance, List<TransactionDto> transactions, List<ScheduledPaymentDto> scheduledPayments);

    List<BankAccountDto> mapToList(List<BankAccount> bankAccounts);

    default Page<BankAccountDto> mapToPageDto(Page<BankAccount> bankAccounts) {
        return new PageImpl<>(mapToList(bankAccounts.getContent()), bankAccounts.getPageable(), bankAccounts.getTotalPages());
    }
}
