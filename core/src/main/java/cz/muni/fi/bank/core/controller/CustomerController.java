package cz.muni.fi.bank.core.controller;

import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import cz.muni.fi.bank.core.api.post.CustomerCreateDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.facade.CustomerFacade;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = CustomerController.BASE_PATH)
public class CustomerController {

    private final CustomerFacade customerFacade;
    public static final String BASE_PATH = "/api/customers";

    @Autowired
    public CustomerController(CustomerFacade customerFacade) {
        this.customerFacade = customerFacade;
    }

    @GetMapping
    @Operation(summary = "Get all customers",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PostFilter("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND filterObject.email.equals(authentication.principal.name))")
    @PageableAsQueryParam
    public List<CustomerDto> getAll(@Parameter(hidden = true) Pageable pageable) {
        return new ArrayList<>(customerFacade.findAll(pageable).getContent());
    }

    @GetMapping(path = "/{id}")
    @Operation(summary = "Get customer by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_read"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PostAuthorize("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND returnObject.body.email.equals(authentication.principal.name))")
    public ResponseEntity<CustomerDetailedDto> getById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(customerFacade.findById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    @Operation(summary = "Create customer",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "201", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND #customerCreateDto.email.equals(authentication.principal.name))")
    public ResponseEntity<CustomerDto> create(@Valid @RequestBody CustomerCreateDto customerCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(customerFacade.create(customerCreateDto));
    }

    @PatchMapping(path = "/{id}")
    @Operation(summary = "Update customer by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_write"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1') OR (hasAuthority('SCOPE_test_2') AND customerFacade.findById(#id).email.equals(authentication.principal.name))")
    public ResponseEntity<CustomerDto> updateById(@PathVariable("id") Long id, @Valid @RequestBody CustomerUpdateDto customerUpdateDto) {
        try {
            return ResponseEntity.ok(customerFacade.updateById(id, customerUpdateDto));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete customer by ID",
            security = @SecurityRequirement(name = "bearer", scopes = {"test_1"}),
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized - token not provided or not valid", content = @Content()),
                    @ApiResponse(responseCode = "403", description = "Forbidden - token does not have access", content = @Content()),
                    @ApiResponse(responseCode = "404", description = "Not found", content = @Content()),
            }
    )
    @PreAuthorize("hasAuthority('SCOPE_test_1')")
    public ResponseEntity<CustomerDto> deleteById(@PathVariable("id") Long id) {
        try {
            return ResponseEntity.ok(customerFacade.deleteById(id));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
