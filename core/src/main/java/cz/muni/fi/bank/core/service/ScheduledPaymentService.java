package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import cz.muni.fi.bank.core.api.post.ScheduledPaymentCreateDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import cz.muni.fi.bank.core.data.repository.ScheduledPaymentRepository;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ScheduledPaymentService {

    private final ScheduledPaymentRepository scheduledPaymentRepository;

    @Autowired
    public ScheduledPaymentService(ScheduledPaymentRepository scheduledPaymentRepository) {
        this.scheduledPaymentRepository = scheduledPaymentRepository;
    }

    @Transactional(readOnly = true)
    public Page<ScheduledPayment> findAll(Pageable pageable) {
        return scheduledPaymentRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public ScheduledPayment findById(Long id) {
        return scheduledPaymentRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("ScheduledPayment with id: " + id + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<ScheduledPayment> getScheduledPaymentsByBankAccountId(Long accountId) {
        return scheduledPaymentRepository.getScheduledPaymentsByBankAccountId(accountId);
    }

    public ScheduledPayment create(ScheduledPaymentCreateDto scheduledPaymentCreateDto, BankAccount bankAccount) {
        ScheduledPayment scheduledPayment = new ScheduledPayment();
        scheduledPayment.setBankAccount(bankAccount);
        scheduledPayment.setDueDate(scheduledPaymentCreateDto.getDueDate());
        scheduledPayment.setAmount(scheduledPaymentCreateDto.getAmount());
        scheduledPayment.setFrequencyInDays(scheduledPaymentCreateDto.getFrequencyInDays());
        return scheduledPaymentRepository.save(scheduledPayment);
    }

    public ScheduledPayment updateById(Long id, ScheduledPaymentUpdateDto scheduledPaymentUpdateDto, BankAccount bankAccount) {
        ScheduledPayment scheduledPayment = findById(id);
        scheduledPayment.update(scheduledPaymentUpdateDto, bankAccount);
        try {
            scheduledPaymentRepository.save(scheduledPayment);
            return scheduledPayment;
        } catch (Exception e) {
            throw new ResourceNotFoundException("ScheduledPayment with id: " + id + " was not found.");
        }
    }

    public ScheduledPayment deleteById(Long id) {
        ScheduledPayment scheduledPayment = findById(id);
        try {
            scheduledPaymentRepository.delete(scheduledPayment);
            return scheduledPayment;
        } catch (Exception e) {
            throw new ResourceNotFoundException("ScheduledPayment with id: " + id + " was not found.");
        }
    }
}
