package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import cz.muni.fi.bank.core.api.post.ScheduledPaymentCreateDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.mapper.ScheduledPaymentMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.ScheduledPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ScheduledPaymentFacade {

    private final ScheduledPaymentService scheduledPaymentService;
    private final ScheduledPaymentMapper scheduledPaymentMapper;
    private final BankAccountService bankAccountService;

    @Autowired
    public ScheduledPaymentFacade(ScheduledPaymentService scheduledPaymentService, ScheduledPaymentMapper scheduledPaymentMapper, BankAccountService bankAccountService) {
        this.scheduledPaymentService = scheduledPaymentService;
        this.scheduledPaymentMapper = scheduledPaymentMapper;
        this.bankAccountService = bankAccountService;
    }

    @Transactional(readOnly = true)
    public Page<ScheduledPaymentDto> findAll(Pageable pageable) {
        return scheduledPaymentMapper.mapToPageDto(scheduledPaymentService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public ScheduledPaymentDto findById(Long id) {
        return scheduledPaymentMapper.mapToDto(scheduledPaymentService.findById(id));
    }

    public ScheduledPaymentDto create(ScheduledPaymentCreateDto scheduledPaymentCreateDto) {
        BankAccount bankAccount = bankAccountService.findById(scheduledPaymentCreateDto.getBankAccountId());
        return scheduledPaymentMapper.mapToDto(scheduledPaymentService.create(scheduledPaymentCreateDto, bankAccount));
    }

    public ScheduledPaymentDto updateById(Long id, ScheduledPaymentUpdateDto scheduledPaymentUpdateDto) {
        BankAccount bankAccount = bankAccountService.findById(scheduledPaymentUpdateDto.getBankAccountId());
        return scheduledPaymentMapper.mapToDto(scheduledPaymentService.updateById(id, scheduledPaymentUpdateDto, bankAccount));
    }

    public ScheduledPaymentDto deleteById(Long id) {
        return scheduledPaymentMapper.mapToDto(scheduledPaymentService.deleteById(id));
    }
}
