package cz.muni.fi.bank.core.api.post;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class CustomerCreateDto {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 50)
    private String name;
    @NotNull
    @NotBlank
    @Size(min = 2, max = 50)
    private String surname;
    @NotNull
    @Past
    private LocalDate birthDate;
    @NotNull
    @Email
    private String email;
}
