package cz.muni.fi.bank.core.data.database;

import cz.muni.fi.bank.core.data.enums.TransactionType;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.data.repository.BankAccountRepository;
import cz.muni.fi.bank.core.data.repository.CustomerRepository;
import cz.muni.fi.bank.core.data.repository.ScheduledPaymentRepository;
import cz.muni.fi.bank.core.data.repository.TransactionRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

@Component
public class DatabaseManager {
    @PersistenceContext
    private EntityManager entityManager;

    private final BankAccountRepository bankAccountRepository;
    private final CustomerRepository customerRepository;
    private final ScheduledPaymentRepository scheduledPaymentRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public DatabaseManager(BankAccountRepository bankAccountRepository, CustomerRepository customerRepository, ScheduledPaymentRepository scheduledPaymentRepository, TransactionRepository transactionRepository) {
        this.bankAccountRepository = bankAccountRepository;
        this.customerRepository = customerRepository;
        this.scheduledPaymentRepository = scheduledPaymentRepository;
        this.transactionRepository = transactionRepository;
    }

    @Transactional
    public void clearDatabase() throws DataAccessException {
        transactionRepository.deleteAll();
        scheduledPaymentRepository.deleteAll();
        bankAccountRepository.deleteAll();
        customerRepository.deleteAll();

        resetAutoIncrement("transaction");
        resetAutoIncrement("scheduled_payment");
        resetAutoIncrement("bank_account");
        resetAutoIncrement("customer");
    }

    private void resetAutoIncrement(String tableName) {
        entityManager.createNativeQuery("ALTER SEQUENCE " + tableName + "_id_seq RESTART WITH 1").executeUpdate();
    }

    @Transactional
    public void seedDatabase() throws DataAccessException {

        clearDatabase();

        Customer customer1 = customerRepository.save(new Customer(null, "John", "Doe", LocalDate.of(1990, 1, 1), "john.doe@gmail.com"));
        Customer customer2 = customerRepository.save(new Customer(null, "Jane", "Doe", LocalDate.of(1990, 12, 12), "jane.doe@outlook.com"));

        BankAccount bankAccount1 = bankAccountRepository.save(new BankAccount(null, customer1, "EUR"));
        BankAccount bankAccount2 = bankAccountRepository.save(new BankAccount(null, customer1, "CZK"));
        BankAccount bankAccount3 = bankAccountRepository.save(new BankAccount(null, customer2, "EUR"));

        transactionRepository.save(new Transaction(null, bankAccount1, TransactionType.DEPOSIT, BigDecimal.valueOf(10.10), null));
        transactionRepository.save(new Transaction(null, bankAccount2, TransactionType.DEPOSIT, BigDecimal.valueOf(20.20), null));
        transactionRepository.save(new Transaction(null, bankAccount3, TransactionType.DEPOSIT, BigDecimal.valueOf(30.30), null));
        transactionRepository.save(new Transaction(null, bankAccount2, TransactionType.WITHDRAW, BigDecimal.valueOf(12.3456789), null));
        transactionRepository.save(new Transaction(null, bankAccount3, TransactionType.SENT, BigDecimal.valueOf(-12.3456789), null));
        transactionRepository.save(new Transaction(null, bankAccount1, TransactionType.RECEIVED, BigDecimal.valueOf(12.3456789), null));

        scheduledPaymentRepository.save(new ScheduledPayment(null, bankAccount1, LocalDate.of(2024, 5, 10), BigDecimal.valueOf(10.10), 7));
        scheduledPaymentRepository.save(new ScheduledPayment(null, bankAccount1, LocalDate.of(2025, 1, 1), BigDecimal.valueOf(100000.100000), 365));
        scheduledPaymentRepository.save(new ScheduledPayment(null, bankAccount2, LocalDate.of(2024, 6, 1), BigDecimal.valueOf(20.20), 30));
    }
}
