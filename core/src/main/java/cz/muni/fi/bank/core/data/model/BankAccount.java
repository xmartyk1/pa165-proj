package cz.muni.fi.bank.core.data.model;

import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.io.Serializable;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
public class BankAccount implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;
    @Column(nullable = false)
    private String currency;

    public void update(BankAccountUpdateDto bankAccountCreateDto, Customer customerUpdate) {
        customer = customerUpdate != null ? customerUpdate : customer;
        currency = bankAccountCreateDto.getCurrency() != null ? bankAccountCreateDto.getCurrency() : currency;
    }
}
