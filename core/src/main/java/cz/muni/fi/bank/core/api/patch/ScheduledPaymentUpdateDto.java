package cz.muni.fi.bank.core.api.patch;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class ScheduledPaymentUpdateDto {

    @Min(0)
    @Max(Long.MAX_VALUE)
    private Long bankAccountId;
    @FutureOrPresent
    private LocalDate dueDate;
    @Min(0)
    private BigDecimal amount;
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer frequencyInDays;
}
