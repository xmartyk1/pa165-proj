package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import cz.muni.fi.bank.core.api.post.CustomerCreateDto;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.mapper.BankAccountMapper;
import cz.muni.fi.bank.core.mapper.CustomerMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerFacade {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;
    private final BankAccountService bankAccountService;
    private final BankAccountMapper bankAccountMapper;

    @Autowired
    public CustomerFacade(CustomerService customerService, CustomerMapper customerMapper, BankAccountService bankAccountService, BankAccountMapper bankAccountMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
        this.bankAccountService = bankAccountService;
        this.bankAccountMapper = bankAccountMapper;
    }

    @Transactional(readOnly = true)
    public Page<CustomerDto> findAll(Pageable pageable) {
        return customerMapper.mapToPageDto(customerService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public CustomerDetailedDto findById(Long id) {
        Customer customer = customerService.findById(id);
        List<BankAccountDto> bankAccounts = bankAccountMapper.mapToList(bankAccountService.getBankAccountsByCustomer(customer));
        return customerMapper.mapToDetailedDto(customer, bankAccounts);
    }

    public CustomerDto create(CustomerCreateDto customer) {
        return customerMapper.mapToDto(customerService.create(customer));
    }

    public CustomerDto updateById(Long id, CustomerUpdateDto customerUpdateDto) {
        return customerMapper.mapToDto(customerService.updateById(id, customerUpdateDto));
    }

    public CustomerDto deleteById(Long id) {
        return customerMapper.mapToDto(customerService.deleteById(id));
    }
}
