package cz.muni.fi.bank.core.api.get;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@SuperBuilder
public class CustomerDetailedDto extends CustomerDto {

    private List<BankAccountDto> bankAccounts;
}
