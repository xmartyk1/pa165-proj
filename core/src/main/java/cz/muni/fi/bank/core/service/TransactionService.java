package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.api.external.ConversionResultDto;
import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.data.enums.TransactionType;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.data.repository.TransactionRepository;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class TransactionService {
    @Value("${currency_service.host:localhost}")
    private String currencyServiceHost;

    @Value("${currency_service.port:8082}")
    private int currencyServicePort;

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Transactional(readOnly = true)
    public Page<Transaction> findAll(Pageable pageable) {
        return transactionRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public Transaction findById(Long id) {
        return transactionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Transaction with id: " + id + " was not found."));
    }

    public List<Transaction> getTransactionsByBankAccountId(Long bankAccountId) {
        return transactionRepository.getTransactionsByBankAccountId(bankAccountId);
    }


    public Transaction create(TransactionCreateDto transactionCreateDto, BankAccount bankAccount) {
        Transaction transaction = new Transaction();
        transaction.setBankAccount(bankAccount);
        transaction.setType(TransactionType.valueOf(transactionCreateDto.getType()));
        transaction.setDelta(transactionCreateDto.getDelta());
        return transactionRepository.save(transaction);
    }

    public Transaction updateById(Long id, TransactionUpdateDto transactionUpdateDto, BankAccount bankAccount) {
        Transaction transaction = findById(id);
        transaction.update(transactionUpdateDto, bankAccount);
        try {
            transactionRepository.save(transaction);
            return transaction;
        } catch (Exception e) {
            throw new ResourceNotFoundException("Transaction with id: " + id + " was not found.");
        }
    }

    public Transaction deleteById(Long id) {
        Transaction transaction = findById(id);
        try {
            transactionRepository.delete(transaction);
            return transaction;
        } catch (Exception e) {
            throw new ResourceNotFoundException("Transaction with id: " + id + " was not found.");
        }
    }

    public Transaction transfer(BankAccount fromBankAccount, BankAccount toBankAccount, BigDecimal delta) {

        UriComponentsBuilder builder = UriComponentsBuilder.newInstance()
                .scheme("http")
                .host(currencyServiceHost)
                .port(currencyServicePort)
                .path("/api/currency_conversion/convert")
                .queryParam("baseCurrency", fromBankAccount.getCurrency())
                .queryParam("targetCurrency", toBankAccount.getCurrency())
                .queryParam("amount", delta.toString());

        URI uri = builder.build().toUri();
        System.out.println(uri);
        BigDecimal convertedDelta = Objects.equals(fromBankAccount.getCurrency(), toBankAccount.getCurrency()) ? delta : Objects.requireNonNull(WebClient.create().get()
                .uri(uri)
                .retrieve()
                .bodyToMono(ConversionResultDto.class)
                .doOnError(e -> {
                    throw new ResourceNotFoundException("Currency conversion failed.");
                })
                .block()).getResultAmount();

        Transaction fromTransaction = new Transaction(null, fromBankAccount, TransactionType.SENT, BigDecimal.ZERO.subtract(delta), null);
        Transaction toTransaction = new Transaction(null, toBankAccount, TransactionType.RECEIVED, convertedDelta, null);
        transactionRepository.save(fromTransaction);
        transactionRepository.save(toTransaction);

        return fromTransaction;
    }
}