package cz.muni.fi.bank.core.api.get;

import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ScheduledPaymentDto extends ScheduledPaymentUpdateDto {

    private Long id;
}
