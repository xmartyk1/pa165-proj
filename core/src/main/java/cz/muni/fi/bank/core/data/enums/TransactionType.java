package cz.muni.fi.bank.core.data.enums;


public enum TransactionType {
    WITHDRAW,
    DEPOSIT,
    SENT,
    RECEIVED;

    public static final String REGEX = "^(WITHDRAW|DEPOSIT|SENT|RECEIVED)$";
}
