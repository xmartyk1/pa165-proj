package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.mapper.TransactionMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TransactionFacade {

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final BankAccountService bankAccountService;

    @Autowired
    public TransactionFacade(TransactionService transactionService, TransactionMapper transactionMapper, BankAccountService bankAccountService) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.bankAccountService = bankAccountService;
    }

    @Transactional(readOnly = true)
    public Page<TransactionDto> findAll(Pageable pageable) {
        return transactionMapper.mapToPageDto(transactionService.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public TransactionDto findById(Long id) {
        return transactionMapper.mapToDto(transactionService.findById(id));
    }

    public TransactionDto create(TransactionCreateDto transactionCreateDto) {
        BankAccount bankAccount = bankAccountService.findById(transactionCreateDto.getBankAccountId());
        return transactionMapper.mapToDto(transactionService.create(transactionCreateDto, bankAccount));
    }

    public TransactionDto updateById(Long id, TransactionUpdateDto transactionUpdateDto) {
        BankAccount bankAccount = bankAccountService.findById(transactionUpdateDto.getBankAccountId());
        return transactionMapper.mapToDto(transactionService.updateById(id, transactionUpdateDto, bankAccount));
    }

    public TransactionDto deleteById(Long id) {
        return transactionMapper.mapToDto(transactionService.deleteById(id));
    }
}
