package cz.muni.fi.bank.core.api.patch;

import cz.muni.fi.bank.core.data.enums.TransactionType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class TransactionUpdateDto {

    @Min(1)
    @Max(Long.MAX_VALUE)
    private Long bankAccountId;
    @Pattern(regexp = TransactionType.REGEX)
    private String type;
    private BigDecimal delta;
}
