package cz.muni.fi.bank.core.mapper;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ScheduledPaymentMapper {

    @Mapping(source = "scheduledPayment.bankAccount.id", target = "bankAccountId")
    ScheduledPaymentDto mapToDto(ScheduledPayment scheduledPayment);

    List<ScheduledPaymentDto> mapToList(List<ScheduledPayment> scheduledPayments);

    default Page<ScheduledPaymentDto> mapToPageDto(Page<ScheduledPayment> scheduledPayments) {
        return new PageImpl<>(mapToList(scheduledPayments.getContent()), scheduledPayments.getPageable(), scheduledPayments.getTotalPages());
    }
}
