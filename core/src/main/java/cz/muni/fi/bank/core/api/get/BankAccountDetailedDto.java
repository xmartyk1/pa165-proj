package cz.muni.fi.bank.core.api.get;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class BankAccountDetailedDto extends BankAccountDto {

    private BigDecimal balance;
    private List<TransactionDto> transactions;
    private List<ScheduledPaymentDto> scheduledPayments;
}
