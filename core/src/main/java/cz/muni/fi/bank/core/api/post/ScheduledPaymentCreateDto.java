package cz.muni.fi.bank.core.api.post;

import jakarta.validation.constraints.FutureOrPresent;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ScheduledPaymentCreateDto {

    @NotNull
    @Min(0)
    @Max(Long.MAX_VALUE)
    private Long bankAccountId;
    @NotNull
    @FutureOrPresent
    private LocalDate dueDate;
    @NotNull
    @Min(0)
    private BigDecimal amount;
    @NotNull
    @Min(1)
    @Max(Integer.MAX_VALUE)
    private Integer frequencyInDays;
}
