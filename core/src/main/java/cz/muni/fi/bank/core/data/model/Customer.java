package cz.muni.fi.bank.core.data.model;

import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(nullable = false)
    private LocalDate birthDate;
    @Column(nullable = false, unique = true)
    private String email;

    public void update(CustomerUpdateDto updateCustomer) {
        name = updateCustomer.getName() != null ? updateCustomer.getName() : name;
        surname = updateCustomer.getSurname() != null ? updateCustomer.getSurname() : surname;
        birthDate = updateCustomer.getBirthDate() != null ? updateCustomer.getBirthDate() : birthDate;
        email = updateCustomer.getEmail() != null ? updateCustomer.getEmail() : email;
    }
}
