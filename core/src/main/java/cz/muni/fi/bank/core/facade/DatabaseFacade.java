package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DatabaseFacade {

    private final DatabaseService databaseService;

    @Autowired
    public DatabaseFacade(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public void seedDatabase() {
        databaseService.seedDatabase();
    }

    public void clearDatabase() {
        databaseService.clearDatabase();
    }
}
