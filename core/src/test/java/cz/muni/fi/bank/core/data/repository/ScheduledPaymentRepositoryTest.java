package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import cz.muni.fi.bank.core.util.TestScheduledPaymentDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class ScheduledPaymentRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ScheduledPaymentRepository scheduledPaymentRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();

        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();
    }

    @Test
    void save_whenScheduledPaymentIsValid_savesAndReturnsScheduledPayment() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer.setEmail("213456@muni.cz");
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        ScheduledPayment scheduledPayment = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment.setBankAccount(bankAccount);

        // Act
        ScheduledPayment savedScheduledPayment = scheduledPaymentRepository.save(scheduledPayment);

        // Assert
        assertThat(savedScheduledPayment).isEqualTo(scheduledPayment);
    }

    @Test
    void save_whenScheduledPaymentIsInvalid_throwsException() {
        // Arrange
        ScheduledPayment scheduledPayment = new ScheduledPayment();

        // Act & Assert
        assertThrows(DataIntegrityViolationException.class, () -> scheduledPaymentRepository.save(scheduledPayment));
    }

    @Test
    void findById_whenScheduledPaymentExists_returnsScheduledPayment() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer.setEmail("341431@mail.muni.cz");
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        ScheduledPayment scheduledPayment = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment.setBankAccount(bankAccount);
        entityManager.persist(scheduledPayment);
        entityManager.flush();

        // Act
        ScheduledPayment foundScheduledPayment = scheduledPaymentRepository.findById(scheduledPayment.getId()).orElse(null);

        // Assert
        assertThat(foundScheduledPayment).isEqualTo(scheduledPayment);
    }

    @Test
    void findById_whenScheduledPaymentDoesNotExist_returnsNull() {
        // Act
        ScheduledPayment foundScheduledPayment = scheduledPaymentRepository.findById(999L).orElse(null);

        // Assert
        assertThat(foundScheduledPayment).isNull();
    }

    @Test
    void findAll_whenScheduledPaymentsExist_returnsScheduledPayments() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer.setEmail("343434@mail.muni.cz");
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        ScheduledPayment scheduledPayment1 = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment1.setBankAccount(bankAccount);
        entityManager.persist(scheduledPayment1);

        ScheduledPayment scheduledPayment2 = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment2.setBankAccount(bankAccount);
        entityManager.persist(scheduledPayment2);

        entityManager.flush();

        // Act
        List<ScheduledPayment> foundScheduledPayments = scheduledPaymentRepository.findAll();

        // Assert
        assertThat(foundScheduledPayments).hasSize(2).containsExactlyInAnyOrder(scheduledPayment1, scheduledPayment2);
    }

    @Test
    void findAll_whenNoScheduledPaymentsExist_returnsEmptyList() {
        // Act
        List<ScheduledPayment> foundScheduledPayments = scheduledPaymentRepository.findAll();

        // Assert
        assertThat(foundScheduledPayments).isEmpty();
    }

    @Test
    void delete_whenScheduledPaymentExists_deletesScheduledPayment() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer.setEmail("343434@mail.muni.cz");
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        ScheduledPayment scheduledPayment = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment.setBankAccount(bankAccount);
        entityManager.persist(scheduledPayment);
        entityManager.flush();

        // Act
        scheduledPaymentRepository.delete(scheduledPayment);
        ScheduledPayment foundScheduledPayment = scheduledPaymentRepository.findById(scheduledPayment.getId()).orElse(null);

        // Assert
        assertThat(foundScheduledPayment).isNull();
    }

    @Test
    void delete_whenScheduledPaymentDoesNotExist_doesNotThrowException() {
        // Arrange
        ScheduledPayment scheduledPayment = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();

        // Act & Assert
        assertDoesNotThrow(() -> scheduledPaymentRepository.delete(scheduledPayment));
    }
}