package cz.muni.fi.bank.core.e2e;

import cz.muni.fi.bank.core.api.get.BankAccountDetailedDto;
import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.api.patch.BankAccountUpdateDto;
import cz.muni.fi.bank.core.api.post.BankAccountCreateDto;
import cz.muni.fi.bank.core.controller.BankAccountController;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.JsonUtils;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestEntityManager
@AutoConfigureMockMvc
@Transactional
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class BankAccountIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestEntityManager entityManager;

    private Customer customer;
    private BankAccount bankAccount;

    @BeforeEach
    public void setUp() {

        entityManager.clear();

        customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        bankAccount = entityManager.persist(bankAccount);
        entityManager.flush();
    }

    @Test
    void createBankAccount_validInput_bankAccountCreated() throws Exception {
        // Arrange
        BankAccountCreateDto bankAccountCreateDto = new BankAccountCreateDto();
        bankAccountCreateDto.setCustomerId(customer.getId());
        bankAccountCreateDto.setCurrency("EUR");

        // Act
        MvcResult result = mockMvc.perform(post(BankAccountController.BASE_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(bankAccountCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        BankAccountDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), BankAccountDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getCustomerId()).isEqualTo(bankAccountCreateDto.getCustomerId());
        assertThat(response.getCurrency()).isEqualTo(bankAccountCreateDto.getCurrency());

        BankAccount bankAccountInDb = entityManager.find(BankAccount.class, response.getId());
        assertThat(bankAccountInDb).isNotNull();
        assertThat(bankAccountInDb.getCustomer().getId()).isEqualTo(bankAccountCreateDto.getCustomerId());
        assertThat(bankAccountInDb.getCurrency()).isEqualTo(bankAccountCreateDto.getCurrency());
    }

    @Test
    void getBankAccount_validInput_bankAccountReturned() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(get(BankAccountController.BASE_PATH + "/" + bankAccount.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        BankAccountDetailedDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), BankAccountDetailedDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo(bankAccount.getId());
        assertThat(response.getCurrency()).isEqualTo(bankAccount.getCurrency());
        assertThat(response.getBalance()).isEqualTo(new BigDecimal(0));
        assertThat(response.getScheduledPayments()).isEmpty();
        assertThat(response.getTransactions()).isEmpty();

        BankAccount bankAccountInDb = entityManager.find(BankAccount.class, bankAccount.getId());
        assertThat(bankAccountInDb).isNotNull();
        assertThat(bankAccountInDb.getId()).isEqualTo(bankAccount.getId());
        assertThat(bankAccountInDb.getCurrency()).isEqualTo(bankAccount.getCurrency());
    }

    @Test
    void updateBankAccount_validInput_bankAccountUpdated() throws Exception {
        // Arrange
        BankAccountUpdateDto bankAccountUpdateDto = new BankAccountUpdateDto();
        bankAccountUpdateDto.setCurrency("USD");

        // Act
        MvcResult result = mockMvc.perform(patch(BankAccountController.BASE_PATH + "/" + bankAccount.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(bankAccountUpdateDto)))
                .andExpect(status().isOk()).andReturn();

        // Assert
        BankAccountDetailedDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), BankAccountDetailedDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo(bankAccount.getId());
        assertThat(response.getCurrency()).isEqualTo(bankAccount.getCurrency());

        BankAccount bankAccountInDb = entityManager.find(BankAccount.class, bankAccount.getId());
        assertThat(bankAccountInDb).isNotNull();
        assertThat(bankAccountInDb.getId()).isEqualTo(bankAccount.getId());
        assertThat(bankAccountInDb.getCustomer().getId()).isEqualTo(bankAccount.getCustomer().getId());
        assertThat(bankAccountInDb.getCurrency()).isEqualTo(bankAccountUpdateDto.getCurrency());
    }

    @Test
    void deleteBankAccount_validInput_bankAccountDeleted() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(delete(BankAccountController.BASE_PATH + "/" + bankAccount.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        // Assert
        BankAccountDetailedDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), BankAccountDetailedDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isEqualTo(bankAccount.getId());
        assertThat(response.getCurrency()).isEqualTo(bankAccount.getCurrency());

        BankAccount bankAccountInDb = entityManager.find(BankAccount.class, bankAccount.getId());
        assertThat(bankAccountInDb).isNull();
    }
}