package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.mapper.BankAccountMapper;
import cz.muni.fi.bank.core.mapper.CustomerMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.CustomerService;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class CustomerFacadeTest {

    @Mock
    private CustomerService customerService;

    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private CustomerMapper customerMapper;

    @Mock
    private BankAccountMapper bankAccountMapper;

    @InjectMocks
    private CustomerFacade customerFacade;

    @Test
    void findById_customerFound_returnsCustomer() {
        // Arrange
        Mockito.when(customerService.findById(1L)).thenReturn(TestCustomerDataFactory.customer);
        Mockito.when(bankAccountService.getBankAccountsByCustomer(TestCustomerDataFactory.customer)).thenReturn(TestBankAccountDataFactory.bankAccountList);
        Mockito.when(customerMapper.mapToDetailedDto(any(), any())).thenReturn(TestCustomerDataFactory.customerDetailedDto);
        Mockito.when(bankAccountMapper.mapToList(any())).thenReturn(TestBankAccountDataFactory.bankAccountDtoList);

        // Act
        CustomerDetailedDto foundEntity = customerFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestCustomerDataFactory.customerDetailedDto);
    }

    @Test
    void findById_customerNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(customerService.findById(1L)).thenReturn(null);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(customerFacade.findById(1L))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_customersFound_returnsCustomers() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(customerService.findAll(pageable)).thenReturn(TestCustomerDataFactory.customerPage);
        Mockito.when(customerMapper.mapToPageDto(TestCustomerDataFactory.customerPage)).thenReturn(TestCustomerDataFactory.customerDtoPage);

        // Act
        Page<CustomerDto> foundEntity = customerFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestCustomerDataFactory.customerDtoPage);
        Mockito.verify(customerService, Mockito.times(1)).findAll(pageable);
        Mockito.verify(customerMapper, Mockito.times(1)).mapToPageDto(TestCustomerDataFactory.customerPage);
    }
}
