package cz.muni.fi.bank.core.rest;

import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.controller.BankAccountController;
import cz.muni.fi.bank.core.facade.BankAccountFacade;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BankAccountControllerTest {

    @Mock
    private BankAccountFacade bankAccountFacade;

    @InjectMocks
    private BankAccountController bankAccountController;

    @Test
    void getById_bankAccountFound_returnsBankAccount() {
        // Arrange
        Mockito.when(bankAccountFacade.findById(1L)).thenReturn(TestBankAccountDataFactory.bankAccountDetailedDto);

        // Act
        ResponseEntity<BankAccountDto> foundEntity = bankAccountController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(foundEntity.getBody()).getId()).isEqualTo(TestBankAccountDataFactory.bankAccountDto.getId());
    }

    @Test
    void getAll_bankAccountsFound_returnsBankAccounts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(bankAccountFacade.findAll(pageable)).thenReturn(TestBankAccountDataFactory.bankAccountDtoPage);

        // Act
        List<BankAccountDto> foundEntity = bankAccountController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccountDtoList);
        Mockito.verify(bankAccountFacade, Mockito.times(1)).findAll(pageable);
    }
}
