package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import cz.muni.fi.bank.core.data.repository.ScheduledPaymentRepository;
import cz.muni.fi.bank.core.util.TestScheduledPaymentDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ScheduledPaymentServiceTest {

    @Mock
    private ScheduledPaymentRepository scheduledPaymentRepository;

    @InjectMocks
    private ScheduledPaymentService scheduledPaymentService;

    @Test
    void findById_scheduledPaymentFound_returnsScheduledPayment() {
        // Arrange
        Mockito.when(scheduledPaymentRepository.findById(1L)).thenReturn(Optional.ofNullable(TestScheduledPaymentDataFactory.scheduledPayment));

        // Act
        ScheduledPayment foundEntity = scheduledPaymentService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPayment);
    }

    @Test
    void findAll_scheduledPaymentsFound_returnsScheduledPayments() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(scheduledPaymentRepository.findAll(pageable)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentPage);

        // Act
        Page<ScheduledPayment> foundEntity = scheduledPaymentService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentPage);
    }

    @Test
    void getScheduledPaymentsByAccountId_scheduledPaymentsFound_returnsScheduledPayments() {
        // Arrange
        Mockito.when(scheduledPaymentRepository.getScheduledPaymentsByBankAccountId(1L)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentList);

        // Act
        List<ScheduledPayment> foundEntity = scheduledPaymentService.getScheduledPaymentsByBankAccountId(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentList);
    }
}
