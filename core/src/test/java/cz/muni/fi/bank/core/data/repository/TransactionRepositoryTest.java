package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import cz.muni.fi.bank.core.util.TestTransactionDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class TransactionRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();
    }

    @Test
    void save_whenTransactionIsValid_savesAndReturnsTransaction() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        Transaction transaction = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction.setBankAccount(bankAccount);

        // Act
        Transaction savedTransaction = transactionRepository.save(transaction);

        // Assert
        assertThat(savedTransaction).isEqualTo(transaction);
    }

    @Test
    void save_whenTransactionIsInvalid_throwsException() {
        // Arrange
        Transaction transaction = new Transaction();

        // Act & Assert
        assertThrows(DataIntegrityViolationException.class, () -> transactionRepository.save(transaction));
    }

    @Test
    void findById_whenTransactionExists_returnsTransaction() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        Transaction transaction = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction.setBankAccount(bankAccount);
        entityManager.persist(transaction);
        entityManager.flush();

        // Act
        Transaction foundTransaction = transactionRepository.findById(transaction.getId()).orElse(null);

        // Assert
        assertThat(foundTransaction).isEqualTo(transaction);
    }

    @Test
    void findById_whenTransactionDoesNotExist_returnsNull() {
        // Act
        Transaction foundTransaction = transactionRepository.findById(999L).orElse(null);

        // Assert
        assertThat(foundTransaction).isNull();
    }

    @Test
    void findAll_whenTransactionsExist_returnsTransactions() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        Transaction transaction1 = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction1.setBankAccount(bankAccount);
        entityManager.persist(transaction1);

        Transaction transaction2 = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction2.setBankAccount(bankAccount);
        entityManager.persist(transaction2);

        entityManager.flush();

        // Act
        List<Transaction> foundTransactions = transactionRepository.findAll();

        // Assert
        assertThat(foundTransactions).hasSize(2).containsExactlyInAnyOrder(transaction1, transaction2);
    }

    @Test
    void findAll_whenNoTransactionsExist_returnsEmptyList() {
        // Act
        List<Transaction> foundTransactions = transactionRepository.findAll();

        // Assert
        assertThat(foundTransactions).isEmpty();
    }

    @Test
    void delete_whenTransactionExists_deletesTransaction() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        Transaction transaction = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction.setBankAccount(bankAccount);
        entityManager.persist(transaction);
        entityManager.flush();

        // Act
        transactionRepository.delete(transaction);
        Transaction foundTransaction = transactionRepository.findById(transaction.getId()).orElse(null);

        // Assert
        assertThat(foundTransaction).isNull();
    }

    @Test
    void delete_whenTransactionDoesNotExist_doesNotThrowException() {
        // Arrange
        Transaction transaction = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();

        // Act & Assert
        assertDoesNotThrow(() -> transactionRepository.delete(transaction));
    }
}