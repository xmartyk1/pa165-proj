package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.repository.BankAccountRepository;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class BankAccountServiceTest {

    @Mock
    private BankAccountRepository bankAccountRepository;

    @InjectMocks
    private BankAccountService bankAccountService;

    @Test
    void findById_bankAccountFound_returnsBankAccount() {
        // Arrange
        Mockito.when(bankAccountRepository.findById(1L)).thenReturn(Optional.ofNullable(TestBankAccountDataFactory.bankAccount));

        // Act
        BankAccount foundEntity = bankAccountService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccount);
    }

    @Test
    void findAll_bankAccountsFound_returnsBankAccounts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(bankAccountRepository.findAll(pageable)).thenReturn(TestBankAccountDataFactory.bankAccountPage);

        // Act
        Page<BankAccount> foundEntity = bankAccountService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccountPage);
    }

    @Test
    void getBankAccountsByCustomerId_bankAccountsFound_returnsBankAccounts() {
        // Arrange
        Mockito.when(bankAccountRepository.getBankAccountsByCustomer(TestCustomerDataFactory.customer)).thenReturn(TestBankAccountDataFactory.bankAccountList);

        // Act
        List<BankAccount> foundEntity = bankAccountService.getBankAccountsByCustomer(TestCustomerDataFactory.customer);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccountList);
    }
}
