package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();
    }

    @Test
    void save_whenCustomerIsValid_savesAndReturnsCustomer() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();

        // Act
        Customer savedCustomer = customerRepository.save(customer);

        // Assert
        assertThat(savedCustomer).isEqualTo(customer);
    }

    @Test
    void save_whenCustomerIsInvalid_throwsException() {
        // Arrange
        Customer customer = new Customer();

        // Act & Assert
        assertThrows(DataIntegrityViolationException.class, () -> customerRepository.save(customer));
    }

    @Test
    void findById_whenCustomerExists_returnsCustomer() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        // Act
        Customer foundCustomer = customerRepository.findById(customer.getId()).orElse(null);

        // Assert
        assertThat(foundCustomer).isEqualTo(customer);
    }

    @Test
    void findById_whenCustomerDoesNotExist_returnsNull() {
        // Act
        Customer foundCustomer = customerRepository.findById(999L).orElse(null);

        // Assert
        assertThat(foundCustomer).isNull();
    }

    @Test
    void findAll_whenCustomersExist_returnsCustomers() {
        // Arrange
        Customer customer1 = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer1);

        Customer customer2 = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer2.setEmail("343434@mail.muni.cz");
        entityManager.persist(customer2);

        entityManager.flush();

        // Act
        List<Customer> foundCustomers = customerRepository.findAll();

        // Assert
        assertThat(foundCustomers).hasSize(2).containsExactlyInAnyOrder(customer1, customer2);
    }

    @Test
    void findAll_whenNoCustomersExist_returnsEmptyList() {
        // Act
        List<Customer> foundCustomers = customerRepository.findAll();

        // Assert
        assertThat(foundCustomers).isEmpty();
    }

    @Test
    void delete_whenCustomerExists_deletesCustomer() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        // Act
        customerRepository.delete(customer);
        Customer foundCustomer = customerRepository.findById(customer.getId()).orElse(null);

        // Assert
        assertThat(foundCustomer).isNull();
    }

    @Test
    void delete_whenCustomerDoesNotExist_doesNotThrowException() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();

        // Act & Assert
        assertDoesNotThrow(() -> customerRepository.delete(customer));
    }
}