package cz.muni.fi.bank.core.util;

import cz.muni.fi.bank.core.api.get.BankAccountDetailedDto;
import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.data.model.BankAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class TestBankAccountDataFactory {

    public static BankAccount bankAccount = getBankAccountEntityFactory();

    public static BankAccountDto bankAccountDto = getBankAccountDtoEntityFactory();

    public static BankAccountDetailedDto bankAccountDetailedDto = getBankAccountDetailedDtoEntityFactory();

    public static List<BankAccount> bankAccountList = getBankAccountListEntityFactory();

    public static List<BankAccountDto> bankAccountDtoList = getBankAccountDtoListEntityFactory();

    public static Page<BankAccount> bankAccountPage = getBankAccountPageEntityFactory();

    public static Page<BankAccountDto> bankAccountDtoPage = getBankAccountDtoPageEntityFactory();

    private static Page<BankAccountDto> getBankAccountDtoPageEntityFactory() {
        return new PageImpl<>(bankAccountDtoList);
    }

    private static Page<BankAccount> getBankAccountPageEntityFactory() {
        return new PageImpl<>(bankAccountList);
    }

    private static List<BankAccountDto> getBankAccountDtoListEntityFactory() {

        BankAccountDto.BankAccountDtoBuilder<?, ?> builder1 = BankAccountDto.builder();
        builder1.id(1L);
        builder1.customerId(1L);
        builder1.currency("EUR");
        BankAccountDto b1 = builder1.build();

        BankAccountDto.BankAccountDtoBuilder<?, ?> builder2 = BankAccountDto.builder();
        builder2.id(2L);
        builder2.customerId(1L);
        builder2.currency("CZK");
        BankAccountDto b2 = builder2.build();

        BankAccountDto.BankAccountDtoBuilder<?, ?> builder3 = BankAccountDto.builder();
        builder3.id(3L);
        builder3.customerId(1L);
        builder3.currency("USD");
        BankAccountDto b3 = builder3.build();

        BankAccountDto.BankAccountDtoBuilder<?, ?> builder4 = BankAccountDto.builder();
        builder4.id(4L);
        builder4.customerId(1L);
        builder4.currency("PLN");
        BankAccountDto b4 = builder4.build();

        return List.of(b1, b2, b3, b4);
    }

    private static List<BankAccount> getBankAccountListEntityFactory() {

        BankAccount.BankAccountBuilder builder1 = BankAccount.builder();
        builder1.id(1L);
        builder1.customer(TestCustomerDataFactory.customer);
        builder1.currency("EUR");
        BankAccount b1 = builder1.build();

        BankAccount.BankAccountBuilder builder2 = BankAccount.builder();
        builder2.id(2L);
        builder2.customer(TestCustomerDataFactory.customer);
        builder2.currency("CZK");
        BankAccount b2 = builder2.build();

        BankAccount.BankAccountBuilder builder3 = BankAccount.builder();
        builder3.id(3L);
        builder3.customer(TestCustomerDataFactory.customer);
        builder3.currency("USD");
        BankAccount b3 = builder3.build();

        BankAccount.BankAccountBuilder builder4 = BankAccount.builder();
        builder4.id(4L);
        builder4.customer(TestCustomerDataFactory.customer);
        builder4.currency("PLN");
        BankAccount b4 = builder4.build();

        return List.of(b1, b2, b3, b4);
    }

    private static BankAccountDetailedDto getBankAccountDetailedDtoEntityFactory() {

        BankAccountDetailedDto.BankAccountDetailedDtoBuilder<?, ?> builder1 = BankAccountDetailedDto.builder();
        builder1.id(1L);
        builder1.customerId(1L);
        builder1.currency("EUR");
        builder1.balance(BigDecimal.valueOf(100L));
        builder1.transactions(TestTransactionDataFactory.transactionDtoList);
        builder1.scheduledPayments(TestScheduledPaymentDataFactory.scheduledPaymentDtoList);

        return builder1.build();
    }

    private static BankAccountDto getBankAccountDtoEntityFactory() {

        BankAccountDto.BankAccountDtoBuilder<?, ?> builder1 = BankAccountDto.builder();
        builder1.id(1L);
        builder1.customerId(1L);
        builder1.currency("EUR");

        return builder1.build();
    }

    private static BankAccount getBankAccountEntityFactory() {

        BankAccount bankAccount = getBankAccountEntityWithoutIdFactory();
        bankAccount.setId(1L);

        return bankAccount;
    }

    public static BankAccount getBankAccountEntityWithoutIdFactory() {

        BankAccount bankAccount = new BankAccount();
        bankAccount.setCurrency("CZK");
        bankAccount.setCurrency("EUR");

        return bankAccount;
    }
}
