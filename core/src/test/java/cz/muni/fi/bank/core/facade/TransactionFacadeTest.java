package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.mapper.TransactionMapper;
import cz.muni.fi.bank.core.service.TransactionService;
import cz.muni.fi.bank.core.util.TestTransactionDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class TransactionFacadeTest {

    @Mock
    private TransactionService transactionService;

    @Mock
    private TransactionMapper transactionMapper;

    @InjectMocks
    private TransactionFacade transactionFacade;

    @Test
    void findById_transactionFound_returnsTransaction() {
        // Arrange
        Mockito.when(transactionService.findById(1L)).thenReturn(TestTransactionDataFactory.transaction);
        Mockito.when(transactionMapper.mapToDto(any())).thenReturn(TestTransactionDataFactory.transactionDto);

        // Act
        TransactionDto foundEntity = transactionFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transactionDto);
    }

    @Test
    void findById_transactionNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(transactionService.findById(1L)).thenReturn(null);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(transactionFacade.findById(1L))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_transactionsFound_returnsTransactions() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(transactionService.findAll(pageable)).thenReturn(TestTransactionDataFactory.transactionPage);
        Mockito.when(transactionMapper.mapToPageDto(TestTransactionDataFactory.transactionPage)).thenReturn(TestTransactionDataFactory.transactionDtoPage);

        // Act
        Page<TransactionDto> foundEntity = transactionFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transactionDtoPage);
        Mockito.verify(transactionService, Mockito.times(1)).findAll(pageable);
        Mockito.verify(transactionMapper, Mockito.times(1)).mapToPageDto(TestTransactionDataFactory.transactionPage);
    }
}
