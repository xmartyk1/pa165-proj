package cz.muni.fi.bank.core.data.repository;

import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DataJpaTest
public class BankAccountRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @BeforeEach
    public void setUp() {
        entityManager.clear();
    }

    @Test
    void save_whenBankAccountIsValid_savesAndReturnsBankAccount() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);

        // Act
        BankAccount savedBankAccount = bankAccountRepository.save(bankAccount);

        // Assert
        assertThat(savedBankAccount).isEqualTo(bankAccount);
    }

    @Test
    void save_whenBankAccountIsInvalid_throwsException() {
        // Arrange
        BankAccount bankAccount = new BankAccount();

        // Act & Assert
        assertThrows(DataIntegrityViolationException.class, () -> bankAccountRepository.save(bankAccount));
    }

    @Test
    void findById_whenBankAccountExists_returnsBankAccount() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        // Act
        BankAccount foundBankAccount = bankAccountRepository.findById(bankAccount.getId()).orElse(null);

        // Assert
        assertThat(foundBankAccount).isEqualTo(bankAccount);
    }

    @Test
    void findById_whenBankAccountDoesNotExist_returnsNull() {
        // Act
        BankAccount foundBankAccount = bankAccountRepository.findById(999L).orElse(null);

        // Assert
        assertThat(foundBankAccount).isNull();
    }

    @Test
    void findAll_whenBankAccountsExist_returnsBankAccounts() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount1 = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount1.setCustomer(customer);
        entityManager.persist(bankAccount1);

        BankAccount bankAccount2 = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount2.setCustomer(customer);
        entityManager.persist(bankAccount2);

        entityManager.flush();

        // Act
        List<BankAccount> foundBankAccounts = bankAccountRepository.findAll();

        // Assert
        assertThat(foundBankAccounts).hasSize(2).containsExactlyInAnyOrder(bankAccount1, bankAccount2);
    }

    @Test
    void findAll_whenNoBankAccountsExist_returnsEmptyList() {
        // Act
        List<BankAccount> foundBankAccounts = bankAccountRepository.findAll();

        // Assert
        assertThat(foundBankAccounts).isEmpty();
    }

    @Test
    void delete_whenBankAccountExists_deletesBankAccount() {
        // Arrange
        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        entityManager.persist(bankAccount);
        entityManager.flush();

        // Act
        bankAccountRepository.delete(bankAccount);
        BankAccount foundBankAccount = bankAccountRepository.findById(bankAccount.getId()).orElse(null);

        // Assert
        assertThat(foundBankAccount).isNull();
    }

    @Test
    void delete_whenBankAccountDoesNotExist_doesNotThrowException() {
        // Arrange
        BankAccount bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();

        // Act & Assert
        assertDoesNotThrow(() -> bankAccountRepository.delete(bankAccount));
    }
}