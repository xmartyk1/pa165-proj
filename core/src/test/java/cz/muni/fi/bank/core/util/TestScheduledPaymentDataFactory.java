package cz.muni.fi.bank.core.util;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Component
public class TestScheduledPaymentDataFactory {
    public static ScheduledPayment scheduledPayment = getScheduledPaymentEntityFactory();

    public static ScheduledPaymentDto scheduledPaymentDto = getScheduledPaymentDtoEntityFactory();

    public static List<ScheduledPayment> scheduledPaymentList = getScheduledPaymentListEntityFactory();

    public static List<ScheduledPaymentDto> scheduledPaymentDtoList = getScheduledPaymentDtoListEntityFactory();

    public static Page<ScheduledPayment> scheduledPaymentPage = getScheduledPaymentPageEntityFactory();

    public static Page<ScheduledPaymentDto> scheduledPaymentDtoPage = getScheduledPaymentDtoPageEntityFactory();

    private static Page<ScheduledPaymentDto> getScheduledPaymentDtoPageEntityFactory() {
        return new PageImpl<>(scheduledPaymentDtoList);
    }

    private static Page<ScheduledPayment> getScheduledPaymentPageEntityFactory() {
        return new PageImpl<>(scheduledPaymentList);
    }

    private static List<ScheduledPaymentDto> getScheduledPaymentDtoListEntityFactory() {

        ScheduledPaymentDto.ScheduledPaymentDtoBuilder<?, ?> builder1 = ScheduledPaymentDto.builder();
        builder1.id(1L);
        builder1.bankAccountId(1L);
        builder1.dueDate(LocalDate.of(1234, 11, 11));
        builder1.amount(BigDecimal.valueOf(1L));
        builder1.frequencyInDays(1);
        ScheduledPaymentDto s1 = builder1.build();

        ScheduledPaymentDto.ScheduledPaymentDtoBuilder<?, ?> builder2 = ScheduledPaymentDto.builder();
        builder2.id(2L);
        builder2.bankAccountId(1L);
        builder2.dueDate(LocalDate.of(2222, 11, 22));
        builder2.amount(BigDecimal.valueOf(2L));
        builder2.frequencyInDays(7);
        ScheduledPaymentDto s2 = builder2.build();

        ScheduledPaymentDto.ScheduledPaymentDtoBuilder<?, ?> builder3 = ScheduledPaymentDto.builder();
        builder3.id(3L);
        builder3.bankAccountId(1L);
        builder3.dueDate(LocalDate.of(3333, 11, 22));
        builder3.amount(BigDecimal.valueOf(3L));
        builder3.frequencyInDays(30);
        ScheduledPaymentDto s3 = builder3.build();

        return List.of(s1, s2, s3);
    }

    private static List<ScheduledPayment> getScheduledPaymentListEntityFactory() {

        ScheduledPayment.ScheduledPaymentBuilder builder1 = ScheduledPayment.builder();
        builder1.id(1L);
        builder1.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder1.dueDate(LocalDate.of(1234, 11, 11));
        builder1.amount(BigDecimal.valueOf(1L));
        builder1.frequencyInDays(1);
        ScheduledPayment s1 = builder1.build();

        ScheduledPayment.ScheduledPaymentBuilder builder2 = ScheduledPayment.builder();
        builder2.id(2L);
        builder2.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder2.dueDate(LocalDate.of(2222, 11, 22));
        builder2.amount(BigDecimal.valueOf(2L));
        builder2.frequencyInDays(7);
        ScheduledPayment s2 = builder2.build();

        ScheduledPayment.ScheduledPaymentBuilder builder3 = ScheduledPayment.builder();
        builder3.id(3L);
        builder3.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder3.dueDate(LocalDate.of(3333, 11, 22));
        builder3.amount(BigDecimal.valueOf(3L));
        builder3.frequencyInDays(30);
        ScheduledPayment s3 = builder3.build();

        return List.of(s1, s2, s3);
    }

    private static ScheduledPaymentDto getScheduledPaymentDtoEntityFactory() {

        ScheduledPaymentDto.ScheduledPaymentDtoBuilder<?, ?> builder1 = ScheduledPaymentDto.builder();
        builder1.id(1L);
        builder1.bankAccountId(1L);
        builder1.dueDate(LocalDate.of(1234, 11, 11));
        builder1.amount(BigDecimal.valueOf(1L));
        builder1.frequencyInDays(1);

        return builder1.build();
    }

    private static ScheduledPayment getScheduledPaymentEntityFactory() {

        ScheduledPayment scheduledPayment = getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment.setId(1L);

        return scheduledPayment;
    }

    public static ScheduledPayment getScheduledPaymentEntityWithoutIdFactory() {

        ScheduledPayment.ScheduledPaymentBuilder builder1 = ScheduledPayment.builder();
        builder1.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder1.dueDate(LocalDate.of(1234, 11, 11));
        builder1.amount(BigDecimal.valueOf(1L));
        builder1.frequencyInDays(1);

        return builder1.build();
    }
}
