package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.mapper.ScheduledPaymentMapper;
import cz.muni.fi.bank.core.service.ScheduledPaymentService;
import cz.muni.fi.bank.core.util.TestScheduledPaymentDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class ScheduledPaymentFacadeTest {

    @Mock
    private ScheduledPaymentService scheduledPaymentService;

    @Mock
    private ScheduledPaymentMapper scheduledPaymentMapper;

    @InjectMocks
    private ScheduledPaymentFacade scheduledPaymentFacade;

    @Test
    void findById_scheduledPaymentFound_returnsScheduledPayment() {
        // Arrange
        Mockito.when(scheduledPaymentService.findById(1L)).thenReturn(TestScheduledPaymentDataFactory.scheduledPayment);
        Mockito.when(scheduledPaymentMapper.mapToDto(any())).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentDto);

        // Act
        ScheduledPaymentDto foundEntity = scheduledPaymentFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentDto);
    }

    @Test
    void findById_scheduledPaymentNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(scheduledPaymentService.findById(1L)).thenReturn(null);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(scheduledPaymentFacade.findById(1L))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_scheduledPaymentsFound_returnsScheduledPayments() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(scheduledPaymentService.findAll(pageable)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentPage);
        Mockito.when(scheduledPaymentMapper.mapToPageDto(TestScheduledPaymentDataFactory.scheduledPaymentPage)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentDtoPage);

        // Act
        Page<ScheduledPaymentDto> foundEntity = scheduledPaymentFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentDtoPage);
        Mockito.verify(scheduledPaymentService, Mockito.times(1)).findAll(pageable);
        Mockito.verify(scheduledPaymentMapper, Mockito.times(1)).mapToPageDto(TestScheduledPaymentDataFactory.scheduledPaymentPage);
    }
}
