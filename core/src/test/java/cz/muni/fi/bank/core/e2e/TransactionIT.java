package cz.muni.fi.bank.core.e2e;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.api.patch.TransactionUpdateDto;
import cz.muni.fi.bank.core.api.post.TransactionCreateDto;
import cz.muni.fi.bank.core.controller.TransactionController;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.JsonUtils;
import cz.muni.fi.bank.core.util.TestTransactionDataFactory;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestEntityManager
@AutoConfigureMockMvc
@Transactional
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class TransactionIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestEntityManager entityManager;

    private BankAccount bankAccount;
    private Transaction transaction;

    @BeforeEach
    public void setUp() {

        entityManager.clear();

        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        bankAccount = entityManager.persist(bankAccount);
        entityManager.flush();

        transaction = TestTransactionDataFactory.getTransactionEntityWithoutIdFactory();
        transaction.setBankAccount(bankAccount);
        transaction = entityManager.persist(transaction);
        entityManager.flush();
    }

    @Test
    void createTransaction_validInput_transactionCreated() throws Exception {
        // Arrange
        TransactionCreateDto transactionCreateDto = new TransactionCreateDto();
        transactionCreateDto.setBankAccountId(bankAccount.getId());
        transactionCreateDto.setType("DEPOSIT");
        transactionCreateDto.setDelta(new BigDecimal(-100));

        // Act
        MvcResult result = mockMvc.perform(post(TransactionController.BASE_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(transactionCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        TransactionDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), TransactionDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(transactionCreateDto.getBankAccountId());
        assertThat(response.getType()).isEqualTo(transactionCreateDto.getType());
        assertThat(response.getDelta()).isEqualTo(transactionCreateDto.getDelta());

        Transaction transactionInDb = entityManager.find(Transaction.class, response.getId());
        assertThat(transactionInDb).isNotNull();
        assertThat(transactionInDb.getType().toString()).isEqualTo(transactionCreateDto.getType());
        assertThat(transactionInDb.getDelta()).isEqualTo(transactionCreateDto.getDelta());
    }

    @Test
    void getTransaction_validInput_transactionReturned() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(get(TransactionController.BASE_PATH + "/" + transaction.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        TransactionDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), TransactionDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(transaction.getBankAccount().getId());
        assertThat(response.getType()).isEqualTo(transaction.getType().toString());
        assertThat(response.getDelta()).isEqualTo(transaction.getDelta());

        Transaction transactionInDb = entityManager.find(Transaction.class, response.getId());
        assertThat(transactionInDb).isNotNull();
        assertThat(transactionInDb.getType().toString()).isEqualTo(transaction.getType().toString());
        assertThat(transactionInDb.getDelta()).isEqualTo(transaction.getDelta());
    }

    @Test
    void updateTransaction_validInput_transactionUpdated() throws Exception {
        // Arrange
        TransactionUpdateDto transactionUpdateDto = new TransactionUpdateDto();
        transactionUpdateDto.setBankAccountId(bankAccount.getId());
        transactionUpdateDto.setType("DEPOSIT");
        transactionUpdateDto.setDelta(new BigDecimal(-100));

        // Act
        MvcResult result = mockMvc.perform(patch(TransactionController.BASE_PATH + "/" + transaction.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(transactionUpdateDto)))
                .andExpect(status().isOk()).andReturn();

        // Assert
        TransactionDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), TransactionDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(transactionUpdateDto.getBankAccountId());
        assertThat(response.getType()).isEqualTo(transactionUpdateDto.getType());
        assertThat(response.getDelta()).isEqualTo(transactionUpdateDto.getDelta());

        Transaction transactionInDb = entityManager.find(Transaction.class, response.getId());
        assertThat(transactionInDb).isNotNull();
        assertThat(transactionInDb.getType().toString()).isEqualTo(transactionUpdateDto.getType());
        assertThat(transactionInDb.getDelta()).isEqualTo(transactionUpdateDto.getDelta());
    }

    @Test
    void deleteTransaction_validInput_transactionDeleted() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(delete(TransactionController.BASE_PATH + "/" + transaction.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        // Assert
        TransactionDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), TransactionDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(transaction.getBankAccount().getId());
        assertThat(response.getType()).isEqualTo(transaction.getType().toString());
        assertThat(response.getDelta()).isEqualTo(transaction.getDelta());

        Transaction transactionInDb = entityManager.find(Transaction.class, response.getId());
        assertThat(transactionInDb).isNull();
    }
}