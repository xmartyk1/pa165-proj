package cz.muni.fi.bank.core.rest;

import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.controller.CustomerController;
import cz.muni.fi.bank.core.facade.CustomerFacade;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    private CustomerFacade customerFacade;

    @InjectMocks
    private CustomerController customerController;

    @Test
    void getById_customerFound_returnsCustomer() {
        // Arrange
        Mockito.when(customerFacade.findById(1L)).thenReturn(TestCustomerDataFactory.customerDetailedDto);

        // Act
        ResponseEntity<CustomerDetailedDto> foundEntity = customerController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(foundEntity.getBody()).isEqualTo(TestCustomerDataFactory.customerDetailedDto);
    }

    @Test
    void getAll_customersFound_returnsCustomers() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(customerFacade.findAll(pageable)).thenReturn(TestCustomerDataFactory.customerDtoPage);

        // Act
        List<CustomerDto> foundEntity = customerController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestCustomerDataFactory.customerDtoList);
        Mockito.verify(customerFacade, Mockito.times(1)).findAll(pageable);
    }
}
