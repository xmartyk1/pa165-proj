package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.data.model.Transaction;
import cz.muni.fi.bank.core.data.repository.TransactionRepository;
import cz.muni.fi.bank.core.util.TestTransactionDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    void findById_transactionFound_returnsTransaction() {
        // Arrange
        Mockito.when(transactionRepository.findById(1L)).thenReturn(Optional.ofNullable(TestTransactionDataFactory.transaction));

        // Act
        Transaction foundEntity = transactionService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transaction);
    }

    @Test
    void findAll_transactionsFound_returnsTransactions() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(transactionRepository.findAll(pageable)).thenReturn(TestTransactionDataFactory.transactionPage);

        // Act
        Page<Transaction> foundEntity = transactionService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transactionPage);
    }

    @Test
    void getAllTransactionsByBankAccountId_transactionsFound_returnsTransactions() {
        // Arrange
        Mockito.when(transactionRepository.getTransactionsByBankAccountId(1L)).thenReturn(TestTransactionDataFactory.transactionList);

        // Act
        List<Transaction> foundEntity = transactionService.getTransactionsByBankAccountId(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transactionList);
    }
}
