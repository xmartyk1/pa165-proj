package cz.muni.fi.bank.core.facade;

import cz.muni.fi.bank.core.api.get.BankAccountDetailedDto;
import cz.muni.fi.bank.core.api.get.BankAccountDto;
import cz.muni.fi.bank.core.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.core.mapper.BankAccountMapper;
import cz.muni.fi.bank.core.mapper.ScheduledPaymentMapper;
import cz.muni.fi.bank.core.mapper.TransactionMapper;
import cz.muni.fi.bank.core.service.BankAccountService;
import cz.muni.fi.bank.core.service.ScheduledPaymentService;
import cz.muni.fi.bank.core.service.TransactionService;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class BankAccountFacadeTest {

    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private BankAccountMapper bankAccountMapper;

    @Mock
    private TransactionService transactionService;

    @Mock
    private TransactionMapper transactionMapper;

    @Mock
    private ScheduledPaymentService scheduledPaymentService;

    @Mock
    private ScheduledPaymentMapper scheduledPaymentMapper;

    @InjectMocks
    private BankAccountFacade bankAccountFacade;

    @Test
    void findById_bankAccountFound_returnsBankAccount() {
        // Arrange
        Mockito.when(bankAccountService.findById(1L)).thenReturn(TestBankAccountDataFactory.bankAccount);
        Mockito.when(bankAccountMapper.mapToDetailedDto(any(), any(), any(), any())).thenReturn(TestBankAccountDataFactory.bankAccountDetailedDto);

        // Act
        BankAccountDetailedDto foundEntity = bankAccountFacade.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccountDetailedDto);
    }

    @Test
    void findById_bankAccountNotFound_throwsResourceNotFoundException() {
        // Arrange
        Mockito.when(bankAccountService.findById(1L)).thenReturn(null);

        // Act & Assert
        assertThrows(ResourceNotFoundException.class, () -> Optional
                .ofNullable(bankAccountFacade.findById(1L))
                .orElseThrow(ResourceNotFoundException::new));
    }

    @Test
    void findAll_bankAccountsFound_returnsBankAccounts() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(bankAccountService.findAll(pageable)).thenReturn(TestBankAccountDataFactory.bankAccountPage);
        Mockito.when(bankAccountMapper.mapToPageDto(TestBankAccountDataFactory.bankAccountPage)).thenReturn(TestBankAccountDataFactory.bankAccountDtoPage);

        // Act
        Page<BankAccountDto> foundEntity = bankAccountFacade.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestBankAccountDataFactory.bankAccountDtoPage);
        Mockito.verify(bankAccountService, Mockito.times(1)).findAll(pageable);
        Mockito.verify(bankAccountMapper, Mockito.times(1)).mapToPageDto(TestBankAccountDataFactory.bankAccountPage);
    }
}
