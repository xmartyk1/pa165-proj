package cz.muni.fi.bank.core.rest;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.controller.ScheduledPaymentController;
import cz.muni.fi.bank.core.facade.ScheduledPaymentFacade;
import cz.muni.fi.bank.core.util.TestScheduledPaymentDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ScheduledPaymentControllerTest {

    @Mock
    private ScheduledPaymentFacade scheduledPaymentFacade;

    @InjectMocks
    private ScheduledPaymentController scheduledPaymentController;

    @Test
    void getById_scheduledPaymentFound_returnsScheduledPayment() {
        // Arrange
        Mockito.when(scheduledPaymentFacade.findById(1L)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentDto);

        // Act
        ResponseEntity<ScheduledPaymentDto> foundEntity = scheduledPaymentController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(foundEntity.getBody()).getId()).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentDto.getId());
    }

    @Test
    void getAll_scheduledPaymentsFound_returnsScheduledPayments() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(scheduledPaymentFacade.findAll(pageable)).thenReturn(TestScheduledPaymentDataFactory.scheduledPaymentDtoPage);

        // Act
        List<ScheduledPaymentDto> foundEntity = scheduledPaymentController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestScheduledPaymentDataFactory.scheduledPaymentDtoList);
        Mockito.verify(scheduledPaymentFacade, Mockito.times(1)).findAll(pageable);
    }
}
