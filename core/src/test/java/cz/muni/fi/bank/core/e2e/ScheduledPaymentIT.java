package cz.muni.fi.bank.core.e2e;

import cz.muni.fi.bank.core.api.get.ScheduledPaymentDto;
import cz.muni.fi.bank.core.api.patch.ScheduledPaymentUpdateDto;
import cz.muni.fi.bank.core.api.post.ScheduledPaymentCreateDto;
import cz.muni.fi.bank.core.controller.ScheduledPaymentController;
import cz.muni.fi.bank.core.data.model.BankAccount;
import cz.muni.fi.bank.core.data.model.ScheduledPayment;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.JsonUtils;
import cz.muni.fi.bank.core.util.TestScheduledPaymentDataFactory;
import cz.muni.fi.bank.core.util.TestBankAccountDataFactory;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestEntityManager
@AutoConfigureMockMvc
@Transactional
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class ScheduledPaymentIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestEntityManager entityManager;

    private BankAccount bankAccount;
    private ScheduledPayment scheduledPayment;

    @BeforeEach
    public void setUp() {

        entityManager.clear();

        Customer customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        entityManager.persist(customer);
        entityManager.flush();

        bankAccount = TestBankAccountDataFactory.getBankAccountEntityWithoutIdFactory();
        bankAccount.setCustomer(customer);
        bankAccount = entityManager.persist(bankAccount);
        entityManager.flush();

        scheduledPayment = TestScheduledPaymentDataFactory.getScheduledPaymentEntityWithoutIdFactory();
        scheduledPayment.setBankAccount(bankAccount);
        scheduledPayment = entityManager.persist(scheduledPayment);
        entityManager.flush();
    }

    @Test
    void createScheduledPayment_validInput_scheduledPaymentCreated() throws Exception {
        // Arrange
        ScheduledPaymentCreateDto scheduledPaymentCreateDto = new ScheduledPaymentCreateDto();
        scheduledPaymentCreateDto.setBankAccountId(bankAccount.getId());
        scheduledPaymentCreateDto.setAmount(new BigDecimal(100));
        scheduledPaymentCreateDto.setDueDate(LocalDate.of(2025, 1, 1));
        scheduledPaymentCreateDto.setFrequencyInDays(30);

        // Act
        MvcResult result = mockMvc.perform(post(ScheduledPaymentController.BASE_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(scheduledPaymentCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        ScheduledPaymentDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), ScheduledPaymentDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(scheduledPaymentCreateDto.getBankAccountId());
        assertThat(response.getAmount()).isEqualTo(scheduledPaymentCreateDto.getAmount());
        assertThat(response.getDueDate()).isEqualTo(scheduledPaymentCreateDto.getDueDate());
        assertThat(response.getFrequencyInDays()).isEqualTo(scheduledPaymentCreateDto.getFrequencyInDays());

        ScheduledPayment scheduledPaymentInDb = entityManager.find(ScheduledPayment.class, response.getId());
        assertThat(scheduledPaymentInDb).isNotNull();
        assertThat(scheduledPaymentInDb.getAmount()).isEqualTo(scheduledPaymentCreateDto.getAmount());
        assertThat(scheduledPaymentInDb.getDueDate()).isEqualTo(scheduledPaymentCreateDto.getDueDate());
        assertThat(scheduledPaymentInDb.getFrequencyInDays()).isEqualTo(scheduledPaymentCreateDto.getFrequencyInDays());
    }

    @Test
    void getScheduledPayment_validInput_scheduledPaymentReturned() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(get(ScheduledPaymentController.BASE_PATH + "/" + scheduledPayment.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        ScheduledPaymentDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), ScheduledPaymentDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(scheduledPayment.getBankAccount().getId());
        assertThat(response.getAmount()).isEqualTo(scheduledPayment.getAmount());
        assertThat(response.getDueDate()).isEqualTo(scheduledPayment.getDueDate());
        assertThat(response.getFrequencyInDays()).isEqualTo(scheduledPayment.getFrequencyInDays());

        ScheduledPayment scheduledPaymentInDb = entityManager.find(ScheduledPayment.class, response.getId());
        assertThat(scheduledPaymentInDb).isNotNull();
        assertThat(scheduledPaymentInDb.getAmount()).isEqualTo(scheduledPayment.getAmount());
        assertThat(scheduledPaymentInDb.getDueDate()).isEqualTo(scheduledPayment.getDueDate());
        assertThat(scheduledPaymentInDb.getFrequencyInDays()).isEqualTo(scheduledPayment.getFrequencyInDays());
    }

    @Test
    void updateScheduledPayment_validInput_scheduledPaymentUpdated() throws Exception {
        // Arrange
        ScheduledPaymentUpdateDto scheduledPaymentUpdateDto = new ScheduledPaymentUpdateDto();
        scheduledPaymentUpdateDto.setBankAccountId(bankAccount.getId());
        scheduledPaymentUpdateDto.setAmount(new BigDecimal(100));
        scheduledPaymentUpdateDto.setDueDate(LocalDate.of(2028, 1, 1));
        scheduledPaymentUpdateDto.setFrequencyInDays(30);

        // Act
        MvcResult result = mockMvc.perform(patch(ScheduledPaymentController.BASE_PATH + "/" + scheduledPayment.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(scheduledPaymentUpdateDto)))
                .andExpect(status().isOk()).andReturn();

        // Assert
        ScheduledPaymentDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), ScheduledPaymentDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(scheduledPayment.getBankAccount().getId());
        assertThat(response.getAmount()).isEqualTo(scheduledPayment.getAmount());
        assertThat(response.getDueDate()).isEqualTo(scheduledPayment.getDueDate());
        assertThat(response.getFrequencyInDays()).isEqualTo(scheduledPayment.getFrequencyInDays());

        ScheduledPayment scheduledPaymentInDb = entityManager.find(ScheduledPayment.class, response.getId());
        assertThat(scheduledPaymentInDb).isNotNull();
        assertThat(scheduledPaymentInDb.getAmount()).isEqualTo(scheduledPayment.getAmount());
        assertThat(scheduledPaymentInDb.getDueDate()).isEqualTo(scheduledPayment.getDueDate());
        assertThat(scheduledPaymentInDb.getFrequencyInDays()).isEqualTo(scheduledPayment.getFrequencyInDays());
    }

    @Test
    void deleteScheduledPayment_validInput_scheduledPaymentDeleted() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(delete(ScheduledPaymentController.BASE_PATH + "/" + scheduledPayment.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        // Assert
        ScheduledPaymentDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), ScheduledPaymentDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getBankAccountId()).isEqualTo(scheduledPayment.getBankAccount().getId());
        assertThat(response.getAmount()).isEqualTo(scheduledPayment.getAmount());
        assertThat(response.getDueDate()).isEqualTo(scheduledPayment.getDueDate());
        assertThat(response.getFrequencyInDays()).isEqualTo(scheduledPayment.getFrequencyInDays());

        ScheduledPayment scheduledPaymentInDb = entityManager.find(ScheduledPayment.class, response.getId());
        assertThat(scheduledPaymentInDb).isNull();
    }
}