package cz.muni.fi.bank.core.rest;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.controller.TransactionController;
import cz.muni.fi.bank.core.facade.TransactionFacade;
import cz.muni.fi.bank.core.util.TestTransactionDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Mock
    private TransactionFacade transactionFacade;

    @InjectMocks
    private TransactionController transactionController;

    @Test
    void getById_transactionFound_returnsTransaction() {
        // Arrange
        Mockito.when(transactionFacade.findById(1L)).thenReturn(TestTransactionDataFactory.transactionDto);

        // Act
        ResponseEntity<TransactionDto> foundEntity = transactionController.getById(1L);

        // Assert
        assertThat(foundEntity.hasBody()).isTrue();
        assertThat(foundEntity.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(200));
        assertThat(Objects.requireNonNull(foundEntity.getBody()).getId()).isEqualTo(TestTransactionDataFactory.transactionDto.getId());
    }

    @Test
    void getAll_transactionsFound_returnsTransactions() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(transactionFacade.findAll(pageable)).thenReturn(TestTransactionDataFactory.transactionDtoPage);

        // Act
        List<TransactionDto> foundEntity = transactionController.getAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestTransactionDataFactory.transactionDtoList);
        Mockito.verify(transactionFacade, Mockito.times(1)).findAll(pageable);
    }
}
