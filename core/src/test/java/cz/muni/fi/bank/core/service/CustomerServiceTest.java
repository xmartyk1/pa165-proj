package cz.muni.fi.bank.core.service;

import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.data.repository.CustomerRepository;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;

    @Test
    void findById_customerFound_returnsCustomer() {
        // Arrange
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.ofNullable(TestCustomerDataFactory.customer));

        // Act
        Customer foundEntity = customerService.findById(1L);

        // Assert
        assertThat(foundEntity).isEqualTo(TestCustomerDataFactory.customer);
    }

    @Test
    void findAll_customersFound_returnsCustomers() {
        // Arrange
        Pageable pageable = Pageable.ofSize(10).withPage(0);
        Mockito.when(customerRepository.findAll(pageable)).thenReturn(TestCustomerDataFactory.customerPage);

        // Act
        Page<Customer> foundEntity = customerService.findAll(pageable);

        // Assert
        assertThat(foundEntity).isEqualTo(TestCustomerDataFactory.customerPage);
        Mockito.verify(customerRepository, Mockito.times(1)).findAll(pageable);
    }
}
