package cz.muni.fi.bank.core.util;

import cz.muni.fi.bank.core.api.get.TransactionDto;
import cz.muni.fi.bank.core.data.enums.TransactionType;
import cz.muni.fi.bank.core.data.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Component
public class TestTransactionDataFactory {

    public static Transaction transaction = getTransactionEntityFactory();

    public static TransactionDto transactionDto = getTransactionDtoEntityFactory();

    public static List<Transaction> transactionList = getTransactionListEntityFactory();

    public static List<TransactionDto> transactionDtoList = getTransactionDtoListEntityFactory();

    public static Page<Transaction> transactionPage = getTransactionPageEntityFactory();

    public static Page<TransactionDto> transactionDtoPage = getTransactionDtoPageEntityFactory();

    private static Page<TransactionDto> getTransactionDtoPageEntityFactory() {
        return new PageImpl<>(transactionDtoList);
    }

    private static Page<Transaction> getTransactionPageEntityFactory() {
        return new PageImpl<>(transactionList);
    }

    private static List<TransactionDto> getTransactionDtoListEntityFactory() {

        TransactionDto.TransactionDtoBuilder<?, ?> builder1 = TransactionDto.builder();
        builder1.id(1L);
        builder1.bankAccountId(1L);
        builder1.type("WITHDRAW");
        builder1.delta(BigDecimal.valueOf(0.1));
        builder1.timestamp(Instant.ofEpochSecond(1));
        TransactionDto t1 = builder1.build();

        TransactionDto.TransactionDtoBuilder<?, ?> builder2 = TransactionDto.builder();
        builder2.id(2L);
        builder2.bankAccountId(1L);
        builder2.type("RECEIVED");
        builder2.delta(BigDecimal.valueOf(0.2));
        builder2.timestamp(Instant.ofEpochSecond(2));
        TransactionDto t2 = builder2.build();

        TransactionDto.TransactionDtoBuilder<?, ?> builder3 = TransactionDto.builder();
        builder3.id(3L);
        builder3.bankAccountId(1L);
        builder3.type("SENT");
        builder3.delta(BigDecimal.valueOf(0.3));
        builder3.timestamp(Instant.ofEpochSecond(3));
        TransactionDto t3 = builder3.build();

        TransactionDto.TransactionDtoBuilder<?, ?> builder4 = TransactionDto.builder();
        builder4.id(4L);
        builder4.bankAccountId(1L);
        builder4.type("DEPOSIT");
        builder4.delta(BigDecimal.valueOf(0.4));
        builder4.timestamp(Instant.ofEpochSecond(4));
        TransactionDto t4 = builder4.build();

        return List.of(t1, t2, t3, t4);
    }

    private static List<Transaction> getTransactionListEntityFactory() {

        Transaction.TransactionBuilder builder1 = Transaction.builder();
        builder1.id(1L);
        builder1.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder1.type(TransactionType.WITHDRAW);
        builder1.delta(BigDecimal.valueOf(0.1));
        builder1.timestamp(Instant.ofEpochSecond(1));
        Transaction t1 = builder1.build();

        Transaction.TransactionBuilder builder2 = Transaction.builder();
        builder2.id(2L);
        builder2.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder2.type(TransactionType.RECEIVED);
        builder2.delta(BigDecimal.valueOf(0.2));
        builder2.timestamp(Instant.ofEpochSecond(2));
        Transaction t2 = builder2.build();

        Transaction.TransactionBuilder builder3 = Transaction.builder();
        builder3.id(3L);
        builder3.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder3.type(TransactionType.SENT);
        builder3.delta(BigDecimal.valueOf(0.3));
        builder3.timestamp(Instant.ofEpochSecond(3));
        Transaction t3 = builder3.build();

        Transaction.TransactionBuilder builder4 = Transaction.builder();
        builder4.id(4L);
        builder4.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder4.type(TransactionType.DEPOSIT);
        builder4.delta(BigDecimal.valueOf(0.4));
        builder4.timestamp(Instant.ofEpochSecond(4));
        Transaction t4 = builder4.build();

        return List.of(t1, t2, t3, t4);
    }

    private static TransactionDto getTransactionDtoEntityFactory() {

        TransactionDto.TransactionDtoBuilder<?, ?> builder1 = TransactionDto.builder();
        builder1.id(1L);
        builder1.bankAccountId(1L);
        builder1.type("WITHDRAW");
        builder1.delta(BigDecimal.valueOf(0.1));
        builder1.timestamp(Instant.ofEpochSecond(123456789));

        return builder1.build();
    }

    private static Transaction getTransactionEntityFactory() {

        Transaction transaction = getTransactionEntityWithoutIdFactory();
        transaction.setId(1L);

        return transaction;
    }

    public static Transaction getTransactionEntityWithoutIdFactory() {

        Transaction.TransactionBuilder builder1 = Transaction.builder();
        builder1.bankAccount(TestBankAccountDataFactory.bankAccount);
        builder1.type(TransactionType.WITHDRAW);
        builder1.delta(BigDecimal.valueOf(0.1));
        builder1.timestamp(Instant.ofEpochSecond(123456789));

        return builder1.build();
    }
}
