package cz.muni.fi.bank.core.util;

import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.data.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class TestCustomerDataFactory {

    public static Customer customer = getCustomerEntityFactory();

    public static CustomerDto customerDto = getCustomerDtoEntityFactory();

    public static CustomerDetailedDto customerDetailedDto = getCustomerDetailedDtoEntityFactory();

    public static List<Customer> customerList = getCustomerListEntityFactory();

    public static List<CustomerDto> customerDtoList = getCustomerDtoListEntityFactory();

    public static Page<Customer> customerPage = getCustomerPageEntityFactory();

    public static Page<CustomerDto> customerDtoPage = getCustomerDtoPageEntityFactory();

    private static Page<CustomerDto> getCustomerDtoPageEntityFactory() {
        return new PageImpl<>(customerDtoList);
    }

    private static Page<Customer> getCustomerPageEntityFactory() {
        return new PageImpl<>(customerList);
    }

    private static List<CustomerDto> getCustomerDtoListEntityFactory() {

        CustomerDto.CustomerDtoBuilder<?, ?> builder1 = CustomerDto.builder();
        builder1.id(1L);
        builder1.name("Jan");
        builder1.surname("Novak");
        builder1.birthDate(LocalDate.of(1999, 2, 4));
        CustomerDto c1 = builder1.build();

        CustomerDto.CustomerDtoBuilder<?, ?> builder2 = CustomerDto.builder();
        builder2.id(2L);
        builder2.name("Jana");
        builder2.surname("Novotna");
        builder2.birthDate(LocalDate.of(1999, 2, 4));
        CustomerDto c2 = builder1.build();

        CustomerDto.CustomerDtoBuilder<?, ?> builder3 = CustomerDto.builder();
        builder3.id(2L);
        builder3.name("Petr");
        builder3.surname("Dvorak");
        builder3.birthDate(LocalDate.of(1988, 7, 20));
        CustomerDto c3 = builder1.build();

        return List.of(c1, c2, c3);
    }

    private static List<Customer> getCustomerListEntityFactory() {

        Customer.CustomerBuilder builder1 = Customer.builder();
        builder1.id(1L);
        builder1.name("Jan");
        builder1.surname("Novak");
        builder1.birthDate(LocalDate.of(1999, 2, 4));
        Customer c1 = builder1.build();

        Customer.CustomerBuilder builder2 = Customer.builder();
        builder2.id(2L);
        builder2.name("Jana");
        builder2.surname("Novotna");
        builder2.birthDate(LocalDate.of(1999, 2, 4));
        Customer c2 = builder1.build();

        Customer.CustomerBuilder builder3 = Customer.builder();
        builder3.id(2L);
        builder3.name("Petr");
        builder3.surname("Dvorak");
        builder3.birthDate(LocalDate.of(1988, 7, 20));
        Customer c3 = builder1.build();

        return List.of(c1, c2, c3);
    }

    private static CustomerDetailedDto getCustomerDetailedDtoEntityFactory() {

        CustomerDetailedDto.CustomerDetailedDtoBuilder<?, ?> customerDto = CustomerDetailedDto.builder();
        customerDto.id(1L);
        customerDto.name("Jan");
        customerDto.surname("Novak");
        customerDto.birthDate(LocalDate.of(1999, 2, 4));
        customerDto.bankAccounts(TestBankAccountDataFactory.bankAccountDtoList);

        return customerDto.build();
    }

    private static CustomerDto getCustomerDtoEntityFactory() {

        CustomerDto.CustomerDtoBuilder<?, ?> customerDto = CustomerDto.builder();
        customerDto.id(1L);
        customerDto.name("Jan");
        customerDto.surname("Novak");
        customerDto.birthDate(LocalDate.of(1999, 2, 4));

        return customerDto.build();
    }

    private static Customer getCustomerEntityFactory() {

        Customer customer = getCustomerEntityWithoutIdFactory();
        customer.setId(1L);

        return customer;
    }

    public static Customer getCustomerEntityWithoutIdFactory() {

        Customer customer = new Customer();
        customer.setName("Jan");
        customer.setSurname("Novak");
        customer.setBirthDate(LocalDate.of(1970, 1, 1));
        customer.setEmail("123456@muni.cz");

        return customer;
    }
}
