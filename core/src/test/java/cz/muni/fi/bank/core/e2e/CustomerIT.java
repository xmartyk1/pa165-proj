package cz.muni.fi.bank.core.e2e;

import cz.muni.fi.bank.core.api.get.CustomerDetailedDto;
import cz.muni.fi.bank.core.api.get.CustomerDto;
import cz.muni.fi.bank.core.api.patch.CustomerUpdateDto;
import cz.muni.fi.bank.core.api.post.CustomerCreateDto;
import cz.muni.fi.bank.core.controller.CustomerController;
import cz.muni.fi.bank.core.data.model.Customer;
import cz.muni.fi.bank.core.util.JsonUtils;
import cz.muni.fi.bank.core.util.TestCustomerDataFactory;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestEntityManager
@AutoConfigureMockMvc
@Transactional
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class CustomerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestEntityManager entityManager;

    private Customer customer;

    @BeforeEach
    public void setUp() {

        entityManager.clear();

        customer = TestCustomerDataFactory.getCustomerEntityWithoutIdFactory();
        customer = entityManager.persist(customer);
        entityManager.flush();
    }

    @Test
    void createCustomer_validInput_customerCreated() throws Exception {
        // Arrange
        CustomerCreateDto customerCreateDto = new CustomerCreateDto();
        customerCreateDto.setName("John");
        customerCreateDto.setSurname("Doe");
        customerCreateDto.setBirthDate(LocalDate.of(1990, 1, 1));
        customerCreateDto.setEmail("123456@mail.muni.cz");

        // Act
        MvcResult result = mockMvc.perform(post(CustomerController.BASE_PATH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(customerCreateDto)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        CustomerDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), CustomerDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getName()).isEqualTo(customerCreateDto.getName());
        assertThat(response.getSurname()).isEqualTo(customerCreateDto.getSurname());
        assertThat(response.getBirthDate()).isEqualTo(customerCreateDto.getBirthDate());

        Customer customerInDb = entityManager.find(Customer.class, response.getId());
        assertThat(customerInDb).isNotNull();
        assertThat(customerInDb.getName()).isEqualTo(customerCreateDto.getName());
        assertThat(customerInDb.getSurname()).isEqualTo(customerCreateDto.getSurname());
        assertThat(customerInDb.getBirthDate()).isEqualTo(customerCreateDto.getBirthDate());
    }

    @Test
    void getCustomer_validInput_customerReturned() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(get(CustomerController.BASE_PATH + "/" + customer.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        CustomerDetailedDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), CustomerDetailedDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getName()).isEqualTo(customer.getName());
        assertThat(response.getSurname()).isEqualTo(customer.getSurname());
        assertThat(response.getBirthDate()).isEqualTo(customer.getBirthDate());
        assertThat(response.getBankAccounts()).isEmpty();

        Customer customerInDb = entityManager.find(Customer.class, response.getId());
        assertThat(customerInDb).isNotNull();
        assertThat(customerInDb.getName()).isEqualTo(customer.getName());
        assertThat(customerInDb.getSurname()).isEqualTo(customer.getSurname());
        assertThat(customerInDb.getBirthDate()).isEqualTo(customer.getBirthDate());
    }

    @Test
    void updateCustomer_validInput_customerUpdated() throws Exception {
        // Arrange
        CustomerUpdateDto customerUpdateDto = new CustomerUpdateDto();
        customerUpdateDto.setName("John");
        customerUpdateDto.setSurname("Doe");
        customerUpdateDto.setBirthDate(LocalDate.of(1990, 1, 1));

        // Act
        MvcResult result = mockMvc.perform(patch(CustomerController.BASE_PATH + "/" + customer.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(JsonUtils.toJson(customerUpdateDto)))
                .andExpect(status().isOk()).andReturn();

        // Assert
        CustomerDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), CustomerDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getName()).isEqualTo(customerUpdateDto.getName());
        assertThat(response.getSurname()).isEqualTo(customerUpdateDto.getSurname());
        assertThat(response.getBirthDate()).isEqualTo(customerUpdateDto.getBirthDate());

        Customer customerInDb = entityManager.find(Customer.class, response.getId());
        assertThat(customerInDb).isNotNull();
        assertThat(customerInDb.getName()).isEqualTo(customerUpdateDto.getName());
        assertThat(customerInDb.getSurname()).isEqualTo(customerUpdateDto.getSurname());
        assertThat(customerInDb.getBirthDate()).isEqualTo(customerUpdateDto.getBirthDate());
    }

    @Test
    void deleteCustomer_validInput_customerDeleted() throws Exception {
        // Act
        MvcResult result = mockMvc.perform(delete(CustomerController.BASE_PATH + "/" + customer.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        // Assert
        CustomerDto response = JsonUtils.fromJson(result.getResponse().getContentAsString(), CustomerDto.class);
        assertThat(response).isNotNull();
        assertThat(response.getId()).isNotNull();
        assertThat(response.getName()).isEqualTo(customer.getName());
        assertThat(response.getSurname()).isEqualTo(customer.getSurname());
        assertThat(response.getBirthDate()).isEqualTo(customer.getBirthDate());

        Customer customerInDb = entityManager.find(Customer.class, response.getId());
        assertThat(customerInDb).isNull();
    }
}
