package cz.muni.fi.bank.currency.service.fetcher;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.api.external.ecb.Eurofxref;
import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.exceptions.ECBExchangeRateWebsiteNotFound;
import org.springframework.http.MediaType;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ECBExchangeRateFetcherService implements IExchangeRateFetcherService {
    @Override
    public ExchangeRateSource getSource() {
        return ExchangeRateSource.ECB;
    }

    @Override
    public List<ExchangeRateDto> fetchExchangeRates() {
        ArrayList<ExchangeRateDto> rates = new ArrayList<>();

        RestClient client = RestClient.builder()
                .messageConverters(converters -> converters.add(new Jaxb2RootElementHttpMessageConverter()))
                .build();
        Eurofxref source = client
                .get()
                .uri("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")
                .accept(MediaType.TEXT_XML)
                .retrieve()
                .body(Eurofxref.class);

        if (source == null) {
            throw new ECBExchangeRateWebsiteNotFound("Eurofxref web not found");
        }

        var items = source.getCube().getCubeItems().getFirst().getCubeItems();
        for (var item : items) {
            var parsedRate = new BigDecimal(item.getRate());

            var rate = new ExchangeRateDto();
            rate.setBaseCurrency("EUR");
            rate.setTargetCurrency(item.getCurrency());
            rate.setRate(parsedRate);
            rate.setTimestamp(LocalDateTime.now());
            rate.setSource(this.getSource());

            rates.add(rate);
            rates.add(rate.reverse());
        }

        return rates;
    }
}
