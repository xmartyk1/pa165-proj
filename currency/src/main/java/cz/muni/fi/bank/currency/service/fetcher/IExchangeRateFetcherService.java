package cz.muni.fi.bank.currency.service.fetcher;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;

import java.util.List;

public interface IExchangeRateFetcherService {
    ExchangeRateSource getSource();

    List<ExchangeRateDto> fetchExchangeRates();
}
