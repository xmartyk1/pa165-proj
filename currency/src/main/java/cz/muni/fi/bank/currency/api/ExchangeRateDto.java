package cz.muni.fi.bank.currency.api;

import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExchangeRateDto {
    private String baseCurrency;
    private String targetCurrency;
    private BigDecimal rate;
    private LocalDateTime timestamp;
    private ExchangeRateSource source;

    public ExchangeRateDto reverse() {
        return new ExchangeRateDto(
                this.targetCurrency,
                this.baseCurrency,
                BigDecimal.ONE.divide(this.getRate(), RoundingMode.HALF_UP),
                this.timestamp,
                this.source
        );
    }
}
