package cz.muni.fi.bank.currency.facade;

import cz.muni.fi.bank.currency.api.ConversionResultDto;
import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.mapper.CurrencyPairMapper;
import cz.muni.fi.bank.currency.mapper.ExchangeRateMapper;
import cz.muni.fi.bank.currency.service.CurrencyPairService;
import cz.muni.fi.bank.currency.service.ExchangeRateService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class CurrencyConversionFacade {
    private final CurrencyPairService currencyPairService;
    private final ExchangeRateService exchangeRateService;
    private final CurrencyPairMapper currencyPairMapper;
    private final ExchangeRateMapper exchangeRateMapper;

    public CurrencyConversionFacade(
            CurrencyPairService currencyPairService,
            ExchangeRateService exchangeRateService,
            CurrencyPairMapper currencyPairMapper,
            ExchangeRateMapper exchangeRateMapper
    ) {
        this.currencyPairService = currencyPairService;
        this.exchangeRateService = exchangeRateService;
        this.currencyPairMapper = currencyPairMapper;
        this.exchangeRateMapper = exchangeRateMapper;
    }

    public ConversionResultDto convertCurrency(String baseCurrency, String targetCurrency, BigDecimal amount) {
        CurrencyPair currencyPair = this.currencyPairService.findCurrencyPair(baseCurrency, targetCurrency);
        BigDecimal resultAmount = exchangeRateService.convertCurrency(currencyPair, amount);
        return new ConversionResultDto(
                baseCurrency, targetCurrency, amount, resultAmount
        );
    }

    public List<CurrencyPairDto> getCurrencies() {
        return this.currencyPairMapper.mapToList(this.currencyPairService.getCurrencyPairs());
    }

    public List<ExchangeRateDto> getExchangeRatesAll() {
        return this.exchangeRateMapper.mapToList(this.exchangeRateService.getExchangeRatesAll());
    }

    public ExchangeRateDto getExchangeRatesCurrent(String baseCurrency, String targetCurrency) {
        CurrencyPair currencyPair = this.currencyPairService.findCurrencyPair(baseCurrency, targetCurrency);
        return this.exchangeRateMapper.mapToDto(this.exchangeRateService.getExchangeRatesCurrent(currencyPair));
    }

    public List<ExchangeRateDto> getExchangeRates(String baseCurrency, String targetCurrency, LocalDateTime startDate, LocalDateTime endDate) throws IllegalStateException {
        if (startDate != null && endDate != null) {
            CurrencyPair currencyPair = this.currencyPairService.findCurrencyPair(baseCurrency, targetCurrency);
            return this.exchangeRateMapper.mapToList(this.exchangeRateService.getExchangeRatesHistory(currencyPair, startDate, endDate));
        }
        throw new IllegalStateException();
    }
}
