package cz.muni.fi.bank.currency.api.external.ecb;

import jakarta.xml.bind.annotation.*;

import java.util.List;
import java.util.SequencedCollection;

@XmlRootElement(name = "Envelope", namespace = "http://www.gesmes.org/xml/2002-08-01")
@XmlAccessorType(XmlAccessType.FIELD)
public class Eurofxref {
    @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
    private Cube cube;

    public Cube getCube() {
        return cube;
    }

    public static class Cube {
        @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref")
        private List<Cube> cubeItems;

        @XmlAttribute(name = "rate")
        private String rate;

        @XmlAttribute(name = "currency")
        private String currency;

        public List<Cube> getCubeItems() {
            return this.cubeItems;
        }

        public String getRate() {
            return this.rate;
        }

        public String getCurrency() {
            return this.currency;
        }
    }
}
