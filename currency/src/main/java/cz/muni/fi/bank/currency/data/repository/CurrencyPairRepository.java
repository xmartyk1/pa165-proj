package cz.muni.fi.bank.currency.data.repository;

import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CurrencyPairRepository extends JpaRepository<CurrencyPair, Long> {
    @Query("SELECT cp FROM CurrencyPair cp WHERE cp.baseCurrency = :baseCurrency AND cp.targetCurrency = :targetCurrency")
    Optional<CurrencyPair> findCurrencyPair(String baseCurrency, String targetCurrency);
}
