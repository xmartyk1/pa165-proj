package cz.muni.fi.bank.currency.mapper;

import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CurrencyPairMapper {
    CurrencyPairDto mapToDto(CurrencyPair currencyPair);

    List<CurrencyPairDto> mapToList(List<CurrencyPair> currencyPair);
}
