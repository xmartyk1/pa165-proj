package cz.muni.fi.bank.currency.exceptions;

/**
 * Exception thrown when ECB exchange rate website is not found.
 */
public class ECBExchangeRateWebsiteNotFound extends RuntimeException {
    public ECBExchangeRateWebsiteNotFound(String string) {
    }
}
