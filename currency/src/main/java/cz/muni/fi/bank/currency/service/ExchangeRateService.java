package cz.muni.fi.bank.currency.service;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import cz.muni.fi.bank.currency.data.repository.ExchangeRateRepository;
import cz.muni.fi.bank.currency.exceptions.ResourceNotFoundException;
import cz.muni.fi.bank.currency.service.fetcher.ECBExchangeRateFetcherService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class ExchangeRateService {
    private final ECBExchangeRateFetcherService ecbExchangeRateFetcherService;
    private final ExchangeRateRepository exchangeRateRepository;
    private final CurrencyPairRepository currencyPairRepository;

    public ExchangeRateService(
            ECBExchangeRateFetcherService ecbExchangeRateFetcherService,
            ExchangeRateRepository exchangeRateRepository,
            CurrencyPairRepository currencyPairRepository
    ) {
        this.ecbExchangeRateFetcherService = ecbExchangeRateFetcherService;
        this.exchangeRateRepository = exchangeRateRepository;
        this.currencyPairRepository = currencyPairRepository;
    }

    @Transactional(readOnly = true)
    public List<ExchangeRate> getExchangeRatesAll() {
        return this.exchangeRateRepository.findAll();
    }

    @Transactional(readOnly = true)
    public ExchangeRate getExchangeRatesCurrent(CurrencyPair currencyPair) {
        return this.exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(currencyPair)
                .orElseThrow(() -> new ResourceNotFoundException("Exchange rate for pair: " + currencyPair.getBaseCurrency() + "/" + currencyPair.getTargetCurrency() + " was not found."));
    }

    @Transactional(readOnly = true)
    public List<ExchangeRate> getExchangeRatesHistory(CurrencyPair currencyPair, LocalDateTime startDate, LocalDateTime endDate) {
        return this.exchangeRateRepository.findByCurrencyPairAndTimestampBetween(currencyPair, startDate, endDate);
    }

    @Transactional(readOnly = true)
    public BigDecimal convertCurrency(CurrencyPair currencyPair, BigDecimal amount) {
        ExchangeRate exchangeRate = this.getExchangeRatesCurrent(currencyPair);
        return amount.multiply(exchangeRate.getRate());
    }

    @Scheduled(cron = "0 0 * * * *")
    @Transactional
    public void updateExchangeRates() {
        List<ExchangeRateDto> newRatesDto = ecbExchangeRateFetcherService.fetchExchangeRates();
        List<ExchangeRate> newRates = newRatesDto.stream()
                .map(dto -> {
                    CurrencyPair currencyPair = currencyPairRepository.findCurrencyPair(dto.getBaseCurrency(), dto.getTargetCurrency())
                            .orElse(new CurrencyPair(dto.getBaseCurrency(), dto.getTargetCurrency()));
                    currencyPairRepository.save(currencyPair);
                    return new ExchangeRate(
                            currencyPair,
                            dto.getRate(),
                            dto.getTimestamp(),
                            dto.getSource()
                    );
                })
                .toList();
        this.exchangeRateRepository.saveAll(newRates);
    }
}
