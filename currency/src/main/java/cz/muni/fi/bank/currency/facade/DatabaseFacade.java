package cz.muni.fi.bank.currency.facade;

import cz.muni.fi.bank.currency.service.DatabaseService;
import cz.muni.fi.bank.currency.service.ExchangeRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DatabaseFacade {

    private final DatabaseService databaseService;
    private final ExchangeRateService exchangeRateService;


    @Autowired
    public DatabaseFacade(DatabaseService databaseService, ExchangeRateService exchangeRateService) {
        this.databaseService = databaseService;
        this.exchangeRateService = exchangeRateService;
    }

    public void seedDatabase() {
        exchangeRateService.updateExchangeRates();
    }

    public void clearDatabase() {
        databaseService.clearDatabase();
    }
}
