package cz.muni.fi.bank.currency.controller;

import cz.muni.fi.bank.currency.api.ConversionResultDto;
import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.facade.CurrencyConversionFacade;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(path = CurrencyConversionController.BASE_PATH)
public class CurrencyConversionController {
    private final CurrencyConversionFacade currencyConversionFacade;

    public static final String BASE_PATH = "/api/currency_conversion";

    public CurrencyConversionController(CurrencyConversionFacade currencyConversionFacade) {
        this.currencyConversionFacade = currencyConversionFacade;
    }

    @GetMapping("/currencies")
    public List<CurrencyPairDto> getCurrencies() {
        return currencyConversionFacade.getCurrencies();
    }

    @GetMapping("/rates")
    public List<ExchangeRateDto> getExchangeRates() {
        return currencyConversionFacade.getExchangeRatesAll();
    }

    @GetMapping("/rate")
    public ExchangeRateDto getExchangeRates(
            @RequestParam String baseCurrency,
            @RequestParam String targetCurrency
    ) {
        return currencyConversionFacade.getExchangeRatesCurrent(baseCurrency, targetCurrency);
    }

    @GetMapping("/convert")
    public ConversionResultDto convertCurrency(
            @RequestParam String baseCurrency,
            @RequestParam String targetCurrency,
            @RequestParam BigDecimal amount) {
        return currencyConversionFacade.convertCurrency(baseCurrency, targetCurrency, amount);
    }

    @GetMapping("/history")
    public List<ExchangeRateDto> getHistoricalExchangeRates(
            @RequestParam String baseCurrency,
            @RequestParam String targetCurrency,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate
    ) {
        return currencyConversionFacade.getExchangeRates(baseCurrency, targetCurrency, startDate, endDate);
    }
}
