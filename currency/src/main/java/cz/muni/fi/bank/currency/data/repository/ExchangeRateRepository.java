package cz.muni.fi.bank.currency.data.repository;

import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, Long> {
    Optional<ExchangeRate> findFirstByCurrencyPairOrderByTimestampDesc(CurrencyPair currencyPair);

    List<ExchangeRate> findByCurrencyPairAndTimestampBetween(CurrencyPair currencyPair, LocalDateTime startDate, LocalDateTime endDate);
}
