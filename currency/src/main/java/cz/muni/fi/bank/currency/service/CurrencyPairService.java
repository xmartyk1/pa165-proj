package cz.muni.fi.bank.currency.service;

import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import cz.muni.fi.bank.currency.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CurrencyPairService {
    private final CurrencyPairRepository currencyPairRepository;

    public CurrencyPairService(CurrencyPairRepository currencyPairRepository) {
        this.currencyPairRepository = currencyPairRepository;
    }

    @Transactional(readOnly = true)
    public List<CurrencyPair> getCurrencyPairs() {
        return this.currencyPairRepository.findAll();
    }

    @Transactional(readOnly = true)
    public CurrencyPair findCurrencyPair(String baseCurrency, String targetCurrency) {
        return this.currencyPairRepository.findCurrencyPair(baseCurrency, targetCurrency)
                .orElseThrow(() -> new ResourceNotFoundException("Currency pair: " + baseCurrency + "/" + targetCurrency + " was not found."));
    }
}
