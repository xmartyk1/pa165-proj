package cz.muni.fi.bank.currency.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConversionResultDto {
    private String baseCurrency;
    private String targetCurrency;
    private BigDecimal amount;
    private BigDecimal resultAmount;
}
