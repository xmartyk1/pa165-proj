package cz.muni.fi.bank.currency.controller;

import cz.muni.fi.bank.currency.facade.DatabaseFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = DatabaseController.BASE_PATH)
public class DatabaseController {

    public static final String BASE_PATH = "/database";

    private final DatabaseFacade databaseFacade;

    @Autowired
    public DatabaseController(DatabaseFacade databaseFacade) {
        this.databaseFacade = databaseFacade;
    }

    @PostMapping("/seed")
    public void seedDatabase() {
        databaseFacade.seedDatabase();
    }

    @PostMapping("/clear")
    public void clearDatabase() {
        databaseFacade.clearDatabase();
    }
}
