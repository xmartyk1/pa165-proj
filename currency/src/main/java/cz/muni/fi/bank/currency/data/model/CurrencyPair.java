package cz.muni.fi.bank.currency.data.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@Table(
    name = "currency_pairs",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"baseCurrency", "targetCurrency"})}
)
public class CurrencyPair {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String baseCurrency;
    private String targetCurrency;
    @OneToMany(mappedBy = "currencyPair")
    private List<ExchangeRate> exchangeRates;
    public CurrencyPair(String baseCurrency, String targetCurrency) {
        this.baseCurrency = baseCurrency;
        this.targetCurrency = targetCurrency;
    }

    public CurrencyPair reverse() {
        CurrencyPair reversed = new CurrencyPair();
        reversed.setBaseCurrency(this.getTargetCurrency());
        reversed.setTargetCurrency(this.getBaseCurrency());
        return reversed;
    }
}
