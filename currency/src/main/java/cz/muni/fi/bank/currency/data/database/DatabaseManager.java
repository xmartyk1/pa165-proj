package cz.muni.fi.bank.currency.data.database;

import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import cz.muni.fi.bank.currency.data.repository.ExchangeRateRepository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class DatabaseManager {

    private final CurrencyPairRepository currencyPairRepository;
    private final ExchangeRateRepository exchangeRateRepository;

    @Autowired
    public DatabaseManager(CurrencyPairRepository currencyPairRepository, ExchangeRateRepository exchangeRateRepository) {
        this.currencyPairRepository = currencyPairRepository;
        this.exchangeRateRepository = exchangeRateRepository;
    }

    @Transactional
    public void clearDatabase() {
        exchangeRateRepository.deleteAll();
        currencyPairRepository.deleteAll();
    }

    @Transactional
    public void seedDatabase() {

        clearDatabase();

        CurrencyPair currencyPair1 = new CurrencyPair("EUR", "CZK");
        exchangeRateRepository.save(new ExchangeRate(currencyPair1, BigDecimal.valueOf(25.0), LocalDateTime.now(), ExchangeRateSource.ECB));
    }
}
