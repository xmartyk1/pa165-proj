package cz.muni.fi.bank.currency.mapper;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ExchangeRateMapper {
    @Mapping(target = "baseCurrency", source = "currencyPair.baseCurrency")
    @Mapping(target = "targetCurrency", source = "currencyPair.targetCurrency")
    ExchangeRateDto mapToDto(ExchangeRate exchangeRate);

    List<ExchangeRateDto> mapToList(List<ExchangeRate> exchangeRate);
}
