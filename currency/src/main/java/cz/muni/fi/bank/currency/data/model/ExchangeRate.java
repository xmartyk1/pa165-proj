package cz.muni.fi.bank.currency.data.model;

import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"id"})
@Table(name = "exchange_rate")
public class ExchangeRate {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "currency_pair_id")
    private CurrencyPair currencyPair;

    private BigDecimal rate;
    private LocalDateTime timestamp;
    private ExchangeRateSource source;

    public ExchangeRate(CurrencyPair currencyPair, BigDecimal rate, LocalDateTime timestamp, ExchangeRateSource source) {
        this.currencyPair = currencyPair;
        this.rate = rate;
        this.timestamp = timestamp;
        this.source = source;
    }

    public ExchangeRate reverse() {
        ExchangeRate inverse = new ExchangeRate();
        inverse.setCurrencyPair(this.getCurrencyPair().reverse());
        inverse.setRate(BigDecimal.ONE.divide(this.getRate(), RoundingMode.HALF_UP));
        inverse.setSource(this.getSource());
        inverse.setTimestamp(this.getTimestamp());
        return inverse;
    }
}
