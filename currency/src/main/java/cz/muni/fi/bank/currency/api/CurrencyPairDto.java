package cz.muni.fi.bank.currency.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyPairDto {
    private String baseCurrency;
    private String targetCurrency;
}
