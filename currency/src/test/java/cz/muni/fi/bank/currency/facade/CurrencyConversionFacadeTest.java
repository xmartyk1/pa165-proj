package cz.muni.fi.bank.currency.facade;

import cz.muni.fi.bank.currency.api.ConversionResultDto;
import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.mapper.CurrencyPairMapper;
import cz.muni.fi.bank.currency.mapper.ExchangeRateMapper;
import cz.muni.fi.bank.currency.service.CurrencyPairService;
import cz.muni.fi.bank.currency.service.ExchangeRateService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import cz.muni.fi.bank.currency.util.TestCurrencyPairDataFactory;
import cz.muni.fi.bank.currency.util.TestExchangeRateDataFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CurrencyConversionFacadeTest {
	@Mock
	private CurrencyPairService currencyPairService;

	@Mock
	private ExchangeRateService exchangeRateService;

	@Mock
	private CurrencyPairMapper currencyPairMapper;

	@Mock
	private ExchangeRateMapper exchangeRateMapper;

	@InjectMocks
	private CurrencyConversionFacade currencyConversionFacade;

	@Test
	public void convertCurrency_pairExists_returnsConversionResultDto() {

		String baseCurrency = "EUR";
		String targetCurrency = "USD";
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency(baseCurrency);
		cp.setTargetCurrency(targetCurrency);
		BigDecimal rate = new BigDecimal("0.126");

		Mockito.when(currencyPairService.findCurrencyPair(baseCurrency, targetCurrency)).thenReturn(cp);
		Mockito.when(exchangeRateService.convertCurrency(cp, rate)).thenReturn(rate);

		ConversionResultDto result = currencyConversionFacade.convertCurrency(baseCurrency, targetCurrency, rate);

		assertThat(result).isNotNull();
		assertThat(result.getBaseCurrency()).isEqualTo(baseCurrency);
		assertThat(result.getTargetCurrency()).isEqualTo(targetCurrency);
		assertThat(result.getResultAmount()).isEqualTo(rate);
		assertThat(result.getAmount()).isEqualTo(rate);
	}

	@Test
	public void getCurrencies_noPairsExists_returnsEmptyList() {

		Mockito.when(currencyPairService.getCurrencyPairs()).thenReturn(new ArrayList<>());

		List<CurrencyPairDto> result = currencyConversionFacade.getCurrencies();

		assertThat(result).isEmpty();
	}

	@Test
	public void getCurrencies_pairsExists_returnsCurrencyPairList() {

		Mockito.when(currencyPairService.getCurrencyPairs()).thenReturn(TestCurrencyPairDataFactory.cpList);
		Mockito.when(currencyPairMapper.mapToList(TestCurrencyPairDataFactory.cpList)).thenReturn(TestCurrencyPairDataFactory.cpDtoList);

		List<CurrencyPairDto> result = currencyConversionFacade.getCurrencies();

		assertThat(result).hasSize(1);
		assertThat(result.getFirst().getBaseCurrency()).isEqualTo(TestCurrencyPairDataFactory.cpDtoList.getFirst().getBaseCurrency());
		assertThat(result.getFirst().getTargetCurrency()).isEqualTo(TestCurrencyPairDataFactory.cpDtoList.getLast().getTargetCurrency());
	}

	@Test
	public void getExchangeRatesAll_noExchangeRatesExists_returnsEmptyList() {

		Mockito.when(exchangeRateService.getExchangeRatesAll()).thenReturn(new ArrayList<>());

		List<ExchangeRateDto> result = currencyConversionFacade.getExchangeRatesAll();

		assertThat(result).isEmpty();
	}

	@Test
	public void getExchangeRatesAll_exchangeRatesExists_returnsExchangeRateDtoList() {

		Mockito.when(exchangeRateService.getExchangeRatesAll()).thenReturn(TestExchangeRateDataFactory.erList);
		Mockito.when(exchangeRateMapper.mapToList(TestExchangeRateDataFactory.erList)).thenReturn(TestExchangeRateDataFactory.erDtoList);

		List<ExchangeRateDto> result = currencyConversionFacade.getExchangeRatesAll();

		assertThat(result).hasSize(1);
		assertThat(result.getFirst().getRate()).isEqualTo(TestExchangeRateDataFactory.erList.getFirst().getRate());
	}

	@Test
	public void getExchangeRatesCurrent_rateExists_returnsExchangeRateDto() {
		String baseCurrency = "EUR";
		String targetCurrency = "USD";

		Mockito.when(currencyPairService.findCurrencyPair(baseCurrency, targetCurrency)).thenReturn(TestCurrencyPairDataFactory.cp);
		Mockito.when(exchangeRateService.getExchangeRatesCurrent(TestCurrencyPairDataFactory.cp)).thenReturn(TestExchangeRateDataFactory.er);
		Mockito.when(exchangeRateMapper.mapToDto(TestExchangeRateDataFactory.er)).thenReturn(TestExchangeRateDataFactory.erDto);

		ExchangeRateDto result = currencyConversionFacade.getExchangeRatesCurrent(baseCurrency, targetCurrency);

		assertThat(result.getRate()).isEqualTo(TestExchangeRateDataFactory.er.getRate());
	}

	@Test
	public void getExchangeRates_pairExists_returnsExchangeRateDtoList() {

		String baseCurrency = "EUR";
		String targetCurrency = "USD";
		LocalDateTime start = LocalDateTime.now();
		LocalDateTime end = start.plusDays(1);

		Mockito.when(exchangeRateService.getExchangeRatesHistory(TestCurrencyPairDataFactory.cp, start, end)).thenReturn(TestExchangeRateDataFactory.erList);
		Mockito.when(exchangeRateMapper.mapToList(TestExchangeRateDataFactory.erList)).thenReturn(TestExchangeRateDataFactory.erDtoList);
		Mockito.when(currencyPairService.findCurrencyPair(baseCurrency, targetCurrency)).thenReturn(TestCurrencyPairDataFactory.cp);

		List<ExchangeRateDto> result = currencyConversionFacade.getExchangeRates(baseCurrency, targetCurrency, start, end);

		assertThat(result.getFirst().getRate()).isEqualTo(TestExchangeRateDataFactory.erDtoList.getFirst().getRate());
	}
}
