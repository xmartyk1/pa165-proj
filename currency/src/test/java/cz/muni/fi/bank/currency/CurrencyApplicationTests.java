package cz.muni.fi.bank.currency;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CurrencyApplication.class)
class CurrencyApplicationTests {

	@Test
	void contextLoads() {
	}

}
