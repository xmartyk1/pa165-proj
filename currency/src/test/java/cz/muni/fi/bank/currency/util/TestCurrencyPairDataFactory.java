package cz.muni.fi.bank.currency.util;

import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestCurrencyPairDataFactory {

	public static CurrencyPair cp = getCurrencyPairEntityFactory();

	public static CurrencyPairDto cpDto = getCurrencyPairDtoEntityFactory();

	public static List<CurrencyPair> cpList = getCurrencyPairListEntityFactory();

	public static List<CurrencyPairDto> cpDtoList = getCurrencyPairDtoListEntityFactory();

	public static Page<CurrencyPair> cpPage = getCurrencyPairPageEntityFactory();

	public static Page<CurrencyPairDto> cpDtoPage = getCurrencyPairDtoPageEntityFactory();

	private static Page<CurrencyPairDto> getCurrencyPairDtoPageEntityFactory() {
		return new PageImpl<>(cpDtoList);
	}

	private static Page<CurrencyPair> getCurrencyPairPageEntityFactory() {
		return new PageImpl<>(cpList);
	}

	private static List<CurrencyPairDto> getCurrencyPairDtoListEntityFactory() {
		List<CurrencyPairDto> list = new ArrayList<>();
		list.add(
				new CurrencyPairDto(
						"EUR",
						"USD"
				)
		);
		return list;
	}

	private static List<CurrencyPair> getCurrencyPairListEntityFactory() {
		List<CurrencyPair> list = new ArrayList<>();
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("EUR");
		cp.setTargetCurrency("USD");
		list.add(cp);
		return list;
	}

	private static CurrencyPairDto getCurrencyPairDtoEntityFactory() {
		return new CurrencyPairDto(
				"EUR",
				"USD"
		);
	}

	private static CurrencyPair getCurrencyPairEntityFactory() {
		CurrencyPair cp = new CurrencyPair();
		cp.setId(1L);
		cp.setBaseCurrency("AUD");
		cp.setTargetCurrency("DUA");
		return cp;
	}
}
