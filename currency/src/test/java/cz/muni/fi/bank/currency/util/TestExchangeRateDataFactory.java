package cz.muni.fi.bank.currency.util;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class TestExchangeRateDataFactory {

	public static ExchangeRate er = getExchangeRateEntityFactory();

	public static ExchangeRateDto erDto = getExchangeRateDtoEntityFactory();

	public static List<ExchangeRate> erList = getExchangeRateListEntityFactory();

	public static List<ExchangeRateDto> erDtoList = getExchangeRateDtoListEntityFactory();

	public static Page<ExchangeRate> erPage = getExchangeRatePageEntityFactory();

	public static Page<ExchangeRateDto> erDtoPage = getExchangeRateDtoPageEntityFactory();

	private static Page<ExchangeRateDto> getExchangeRateDtoPageEntityFactory() {
		return new PageImpl<>(erDtoList);
	}

	private static Page<ExchangeRate> getExchangeRatePageEntityFactory() {
		return new PageImpl<>(erList);
	}

	private static List<ExchangeRateDto> getExchangeRateDtoListEntityFactory() {
		List<ExchangeRateDto> list = new ArrayList<>();
		list.add(
				new ExchangeRateDto(
						"EUR",
						"USD",
						new BigDecimal("1.00"),
						LocalDateTime.now(),
						ExchangeRateSource.ECB
				)
		);
		return list;
	}

	private static List<ExchangeRate> getExchangeRateListEntityFactory() {
		List<ExchangeRate> list = new ArrayList<>();
		list.add(
				getExchangeRateEntityFactory()
		);
		return list;
	}

	private static ExchangeRateDto getExchangeRateDtoEntityFactory() {
		return new ExchangeRateDto(
				"EUR",
				"USD",
				new BigDecimal("1.00"),
				LocalDateTime.now(),
				ExchangeRateSource.ECB
		);
	}

	private static ExchangeRate getExchangeRateEntityFactory() {
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("EUR");
		cp.setTargetCurrency("USD");
		ExchangeRate er = new ExchangeRate();
		er.setId(1L);
		er.setRate(new BigDecimal("1.00"));
		er.setSource(ExchangeRateSource.ECB);
		er.setTimestamp(LocalDateTime.now());
		er.setCurrencyPair(cp);
		return er;
	}
}
