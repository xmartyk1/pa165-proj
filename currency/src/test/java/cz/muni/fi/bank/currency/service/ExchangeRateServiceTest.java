package cz.muni.fi.bank.currency.service;

import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import cz.muni.fi.bank.currency.data.repository.ExchangeRateRepository;
import cz.muni.fi.bank.currency.service.fetcher.ECBExchangeRateFetcherService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ExchangeRateServiceTest {
	@Mock
	private ExchangeRateRepository exchangeRateRepository;

	@Mock
	private ECBExchangeRateFetcherService ecbExchangeRateFetcherService;

	@InjectMocks
	private ExchangeRateService exchangeRateService;

	@Test
	public void getExchangeRatesAll_noExchangeRates_returnsEmptyList() {

		Mockito.when(exchangeRateRepository.findAll()).thenReturn(new ArrayList<>());

		List<ExchangeRate> result = exchangeRateService.getExchangeRatesAll();

		assertThat(result).isEmpty();
	}

	@Test
	public void getExchangeRatesAll_exchangeRatesExists_returnsNotEmptyList() {

		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("USD");
		cp.setTargetCurrency("EUR");
		BigDecimal rate = new BigDecimal("0.126");
		ExchangeRate exchangeRate = new ExchangeRate();
		exchangeRate.setCurrencyPair(cp);
		exchangeRate.setRate(rate);
		exchangeRate.setTimestamp(LocalDateTime.now());
		exchangeRate.setSource(ExchangeRateSource.ECB);

		Mockito.when(exchangeRateRepository.findAll()).thenReturn(List.of(exchangeRate));

		List<ExchangeRate> result = exchangeRateService.getExchangeRatesAll();

		assertThat(result).isNotEmpty();
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.getFirst()).isEqualTo(exchangeRate);
	}

	@Test
	public void getExchangeRatesCurrent_exchangeRateExists_returnsExchangeRate() {

		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("USD");
		cp.setTargetCurrency("EUR");
		BigDecimal rate = new BigDecimal("0.126");
		ExchangeRate exchangeRate = new ExchangeRate();
		exchangeRate.setCurrencyPair(cp);
		exchangeRate.setRate(rate);
		exchangeRate.setTimestamp(LocalDateTime.now());
		exchangeRate.setSource(ExchangeRateSource.ECB);

		Mockito.when(exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(cp)).thenReturn(Optional.of(exchangeRate));

		ExchangeRate result = exchangeRateService.getExchangeRatesCurrent(cp);

		assertThat(result).isEqualTo(exchangeRate);
	}

	@Test
	public void getExchangeRatesHistory_noHistory_returnsEmptyList() {

		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("USD");
		cp.setTargetCurrency("EUR");
		BigDecimal rate = new BigDecimal("0.126");
		LocalDateTime start = LocalDateTime.now();
		LocalDateTime end = start.plusDays(1).plusHours(1);

		Mockito.when(exchangeRateRepository.findByCurrencyPairAndTimestampBetween(cp, start, end)).thenReturn(new ArrayList<>());

		List<ExchangeRate> result = exchangeRateService.getExchangeRatesHistory(cp, start, end);

		assertThat(result).isEmpty();
	}

	@Test
	public void getExchangeRatesHistory_HistoryExists_returnsNotEmptyList() {

		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("USD");
		cp.setTargetCurrency("EUR");
		BigDecimal rate = new BigDecimal("0.126");
		LocalDateTime start = LocalDateTime.now();
		LocalDateTime end = start.plusDays(1).plusHours(1);
		ExchangeRate exchangeRate = new ExchangeRate();
		exchangeRate.setCurrencyPair(cp);
		exchangeRate.setRate(rate);
		exchangeRate.setTimestamp(start);
		exchangeRate.setSource(ExchangeRateSource.ECB);

		Mockito.when(exchangeRateRepository.findByCurrencyPairAndTimestampBetween(cp, start, end)).thenReturn(List.of(exchangeRate));

		List<ExchangeRate> result = exchangeRateService.getExchangeRatesHistory(cp, start, end);

		assertThat(result).isNotEmpty();
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.getFirst()).isEqualTo(exchangeRate);
	}

	@Test
	public void convertCurrency_currentPairExists_returnsBigDecimal() {
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency("USD");
		cp.setTargetCurrency("EUR");
		BigDecimal rate = new BigDecimal("0.126");
		ExchangeRate exchangeRate = new ExchangeRate();
		exchangeRate.setCurrencyPair(cp);
		exchangeRate.setRate(rate);
		exchangeRate.setTimestamp(LocalDateTime.now());
		exchangeRate.setSource(ExchangeRateSource.ECB);

		Mockito.when(exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(cp)).thenReturn(Optional.of(exchangeRate));

		BigDecimal result = exchangeRateService.convertCurrency(cp, rate);

		assertThat(result).isEqualTo(rate.multiply(rate));
	}

	@Test
	public void updateExchangeRates_ratesLoaded_savesRates() {
		List<ExchangeRate> rates = new ArrayList<>();
		List<ExchangeRateDto> ratesDto = new ArrayList<>();
		Mockito.when(exchangeRateRepository.saveAll(rates)).thenReturn(rates);
		Mockito.when(ecbExchangeRateFetcherService.fetchExchangeRates()).thenReturn(ratesDto);

		exchangeRateService.updateExchangeRates();

		Mockito.verify(exchangeRateRepository).saveAll(rates);
	}
}
