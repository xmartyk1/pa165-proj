package cz.muni.fi.bank.currency.service.fetcher;
import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ECBExchangeRateFetcherServiceTest {

    @Autowired
    private ECBExchangeRateFetcherService service;

    @Test
    void test_fetchExchangeRates_loadsRates() {
        List<ExchangeRateDto> rates = this.service.fetchExchangeRates();
        assertTrue(rates.size() > 5);
        Optional<ExchangeRateDto> eur_czk = rates.stream().filter(it -> it.getBaseCurrency().equals("EUR") && it.getTargetCurrency().equals("CZK")).findFirst();
        assertTrue(eur_czk.isPresent());
        // Assuming EUR/CZK will stay within a sane range :)
        assertEquals(eur_czk.get().getRate().doubleValue(),25, 5);
        assertEquals(eur_czk.get().getRate().doubleValue(),25, 5);
    }
}
