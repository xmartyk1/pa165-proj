package cz.muni.fi.bank.currency.service;

import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class CurrencyPairServiceTest {
	@Mock
	private CurrencyPairRepository currencyPairRepository;

	@InjectMocks
	private CurrencyPairService currencyPairService;

	@Test
	public void getCurrencyPairs_pairsNotFound_returnsEmptyList() {

		Mockito.when(currencyPairRepository.findAll()).thenReturn(new ArrayList<>());

		List<CurrencyPair> currencyPairs = currencyPairService.getCurrencyPairs();

		assertThat(currencyPairs).isEmpty();
	}

	@Test
	public void getCurrencyPairs_pairsFound_returnsCurrencyPairs() {

		String baseCurrency = "USD";
		String targetCurrency = "EUR";
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency(baseCurrency);
		cp.setTargetCurrency(targetCurrency);

		Mockito.when(currencyPairRepository.findAll()).thenReturn(List.of(new CurrencyPair[]{cp}));

		List<CurrencyPair> result = currencyPairService.getCurrencyPairs();

		assertThat(result).isNotEmpty();
		assertThat(result.getFirst().getBaseCurrency()).isEqualTo(baseCurrency);
		assertThat(result.getFirst().getTargetCurrency()).isEqualTo(targetCurrency);
	}

	@Test
	public void findCurrencyPair_pairFound_returnsCurrencyPair() {
		String baseCurrency = "USD";
		String targetCurrency = "EUR";
		CurrencyPair cp = new CurrencyPair();
		cp.setBaseCurrency(baseCurrency);
		cp.setTargetCurrency(targetCurrency);

		Mockito.when(currencyPairRepository.findCurrencyPair(baseCurrency, targetCurrency)).thenReturn(Optional.of(cp));

		CurrencyPair result = currencyPairService.findCurrencyPair(baseCurrency, targetCurrency);

		assertThat(result).isEqualTo(cp);
	}
}
