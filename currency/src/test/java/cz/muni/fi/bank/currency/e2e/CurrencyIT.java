package cz.muni.fi.bank.currency.e2e;

import cz.muni.fi.bank.currency.controller.CurrencyConversionController;
import cz.muni.fi.bank.currency.service.ExchangeRateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;

import static org.hamcrest.Matchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@WithMockUser(username = "admin", authorities = { "SCOPE_test_read", "SCOPE_test_write", "SCOPE_test_1" })
public class CurrencyIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ExchangeRateService exchangeRateService;

    @BeforeEach
    void setUp() {
        // Load latest exchange rates (this would be done by a scheduled task in a real application)
        exchangeRateService.updateExchangeRates();
    }

    @Test
    void getCurrencies_callCurrencies_returnsCurrencies() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(CurrencyConversionController.BASE_PATH + "/currencies")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(60))
                .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.baseCurrency == 'EUR' && @.targetCurrency == 'AUD')]").exists());
    }

    @Test
    void getExchangeRates_callGetExchangeRates_returnsExchangeRates() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(CurrencyConversionController.BASE_PATH + "/rates")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(60))
                .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.baseCurrency == 'EUR' && @.targetCurrency == 'USD')]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[?(@.baseCurrency == 'EUR' && @.targetCurrency == 'USD')].rate").isNotEmpty());
    }

    @Test
    void getExchangeRate_callGetExchangeRate_returnsExchangeRate() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(CurrencyConversionController.BASE_PATH + "/rate")
                        .param("baseCurrency", "EUR")
                        .param("targetCurrency", "USD")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.baseCurrency").value("EUR"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.targetCurrency").value("USD"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.rate").isNumber());
    }

    @Test
    void convertCurrency_callConvertCurrency_returnsConversionResult() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get(CurrencyConversionController.BASE_PATH + "/convert")
                        .param("baseCurrency", "EUR")
                        .param("targetCurrency", "CZK")
                        .param("amount", "100")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.baseCurrency").value("EUR"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.targetCurrency").value("CZK"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.amount").value(100))
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultAmount").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.resultAmount").value(allOf(greaterThan(2000.00), lessThan(3000.00))));
    }

    @Test
    void getHistoricalExchangeRates_callGetHistoricalExchangeRates_returnsHistoricalExchangeRates() throws Exception {
        LocalDateTime now = LocalDateTime.now();
        mockMvc.perform(MockMvcRequestBuilders
                        .get(CurrencyConversionController.BASE_PATH + "/history")
                        .param("baseCurrency", "EUR")
                        .param("targetCurrency", "USD")
                        .param("startDate", now.minusDays(1).toString())
                        .param("endDate", now.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].baseCurrency").value("EUR"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].targetCurrency").value("USD"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].rate").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].timestamp").value(matchesPattern("^\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{1,9}$")));
    }
}