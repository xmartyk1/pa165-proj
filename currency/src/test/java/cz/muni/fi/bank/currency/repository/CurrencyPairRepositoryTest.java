package cz.muni.fi.bank.currency.repository;

import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import cz.muni.fi.bank.currency.util.TestCurrencyPairDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
public class CurrencyPairRepositoryTest {
	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private CurrencyPairRepository currencyPairRepository;

	@BeforeEach
	void initData() {
		currencyPairRepository.saveAll(TestCurrencyPairDataFactory.cpList);
		testEntityManager.flush();
		testEntityManager.clear();
	}

	@Test
	public void findCurrencyPair_pairFound_returnsCurrencyPair() {
		var pair = currencyPairRepository.findCurrencyPair("EUR", "USD");
		assertThat(pair).isPresent();
		assertThat(pair.get().getBaseCurrency()).isEqualTo("EUR");
		assertThat(pair.get().getTargetCurrency()).isEqualTo("USD");
	}

	@Test
	public void findCurrencyPair_pairNotFound_returnsEmpty() {
		var pair = currencyPairRepository.findCurrencyPair("EUR", "MAD");
		assertThat(pair).isEmpty();
	}

	@Test
	public void findAll_pairsFound_returnsPairs() {
		var pairs = currencyPairRepository.findAll();
		assertThat(pairs).hasSize(1);
		assertThat(pairs.getFirst().getBaseCurrency()).isEqualTo("EUR");
		assertThat(pairs.getFirst().getTargetCurrency()).isEqualTo("USD");
	}

	@Test
	public void save_pairSaved_returnsPair() {
		var pair = new CurrencyPair("EUR", "CZK");
		currencyPairRepository.save(pair);
		var savedPair = currencyPairRepository.findCurrencyPair("EUR", "CZK");
		assertThat(savedPair).isPresent();
		assertThat(savedPair.get().getBaseCurrency()).isEqualTo("EUR");
		assertThat(savedPair.get().getTargetCurrency()).isEqualTo("CZK");
	}

	@Test
	public void save_pairAlreadyExists_throwsException() {
		var pair = new CurrencyPair("EUR", "USD");
		assertThatThrownBy(() -> currencyPairRepository.save(pair))
				.isInstanceOf(DataIntegrityViolationException.class);
	}
}
