package cz.muni.fi.bank.currency.repository;

import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.data.model.CurrencyPair;
import cz.muni.fi.bank.currency.data.model.ExchangeRate;
import cz.muni.fi.bank.currency.data.repository.CurrencyPairRepository;
import cz.muni.fi.bank.currency.data.repository.ExchangeRateRepository;
import cz.muni.fi.bank.currency.util.TestExchangeRateDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class ExchangeRateRepositoryTest {
	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private ExchangeRateRepository exchangeRateRepository;

	@Autowired
	private CurrencyPairRepository currencyPairRepository;

	@BeforeEach
	void initData() {
        exchangeRateRepository.saveAll(TestExchangeRateDataFactory.erList);
	}

	@Test
	void findFirstByCurrencyPairOrderByTimestampDesc_exists_returnsRate() {
		var cpResult = currencyPairRepository.findCurrencyPair("EUR", "USD");
		assertThat(cpResult).isPresent();
		var cp = cpResult.get();
		var rate = exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(cp);
		assertThat(rate).isPresent();
		assertThat(rate.get().getCurrencyPair()).isEqualTo(cp);
	}

	@Test
	void save_saveExchangeRate_saves() {
		var cp = new CurrencyPair("EUR", "CZK");
		var er = new ExchangeRate(cp, new BigDecimal("25.5"), LocalDateTime.now(), ExchangeRateSource.ECB);
		exchangeRateRepository.save(er);
		assertThat(exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(cp)).isPresent();
	}

	@Test
	void save_ExchangeRateWithExistingCurrencyPair_saves() {
		var cp = currencyPairRepository.findCurrencyPair("EUR", "USD").get();
		var er = new ExchangeRate(cp, new BigDecimal("1.5"), LocalDateTime.now(), ExchangeRateSource.ECB);
		exchangeRateRepository.save(er);
		assertThat(exchangeRateRepository.findFirstByCurrencyPairOrderByTimestampDesc(cp)).isPresent();
	}
}
