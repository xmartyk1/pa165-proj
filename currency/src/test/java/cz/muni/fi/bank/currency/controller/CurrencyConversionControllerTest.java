package cz.muni.fi.bank.currency.controller;

import cz.muni.fi.bank.currency.api.ConversionResultDto;
import cz.muni.fi.bank.currency.api.CurrencyPairDto;
import cz.muni.fi.bank.currency.api.ExchangeRateDto;
import cz.muni.fi.bank.currency.data.enums.ExchangeRateSource;
import cz.muni.fi.bank.currency.facade.CurrencyConversionFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrencyConversionControllerTest {

	@Mock
	private CurrencyConversionFacade currencyConversionFacade;

	@InjectMocks
	private CurrencyConversionController currencyConversionController;

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getCurrencies_noCurrencies_returnsEmptyList() {
		Mockito.when(currencyConversionFacade.getCurrencies()).thenReturn(List.of(new CurrencyPairDto[]{}));

		List<CurrencyPairDto> result = currencyConversionController.getCurrencies();

		assertThat(result).isEmpty();
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getCurrencies_oneCurrency_returnsEmptyList() {
		Mockito.when(currencyConversionFacade.getCurrencies()).thenReturn(List.of(new CurrencyPairDto[]{new CurrencyPairDto("EUR", "USD")}));

		List<CurrencyPairDto> result = currencyConversionController.getCurrencies();

		assertThat(result).isNotEmpty();
		assertThat(result.getFirst().getBaseCurrency()).isEqualTo("EUR");
		assertThat(result.getFirst().getTargetCurrency()).isEqualTo("USD");
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getExchangeRates_noRates_returnsEmptyList() {
		Mockito.when(currencyConversionFacade.getExchangeRatesAll()).thenReturn(List.of(new ExchangeRateDto[]{}));

		List<ExchangeRateDto> result = currencyConversionController.getExchangeRates();

		assertThat(result).isEmpty();
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getExchangeRates_oneRate_returnsEmptyList() {
		Mockito.when(currencyConversionFacade.getExchangeRatesAll()).thenReturn(List.of(new ExchangeRateDto[]{new ExchangeRateDto("EUR", "USD", new BigDecimal("0.001"), LocalDateTime.now(), ExchangeRateSource.ECB)}));

		List<ExchangeRateDto> result = currencyConversionController.getExchangeRates();

		assertThat(result).isNotEmpty();
		assertThat(result.getFirst().getBaseCurrency()).isEqualTo("EUR");
		assertThat(result.getFirst().getTargetCurrency()).isEqualTo("USD");
		assertThat(result.getFirst().getRate()).isEqualTo(new BigDecimal("0.001"));
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void convertCurrency_validConversion_returnsRightAmount() {
		String baseCurrency = "EUR";
		String targetCurrency = "USD";
		BigDecimal amount = new BigDecimal("10.001");
		BigDecimal resultAmount = new BigDecimal("0.001");
		ConversionResultDto resultDto = new ConversionResultDto(baseCurrency, targetCurrency, amount, resultAmount);

		Mockito.when(currencyConversionFacade.convertCurrency(baseCurrency, targetCurrency, amount)).thenReturn(resultDto);

		ConversionResultDto res = currencyConversionController.convertCurrency(baseCurrency, targetCurrency, amount);

		assertThat(res.getResultAmount()).isEqualTo(resultAmount);
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getHistoricalExchangeRates_noHistory_returnsEmptyList() {
		String baseCurrency = "EUR";
		String targetCurrency = "USD";
		LocalDateTime start = LocalDateTime.now();
		LocalDateTime end = start.plusDays(1);

		Mockito.when(currencyConversionFacade.getExchangeRates(baseCurrency, targetCurrency, start, end)).thenReturn(List.of(new ExchangeRateDto[]{}));

		List<ExchangeRateDto> result = currencyConversionController.getHistoricalExchangeRates(baseCurrency, targetCurrency, start, end);

		assertThat(result).isEmpty();
	}

	@Test
	@ExtendWith(MockitoExtension.class)
	public void getHistoricalExchangeRates_oneHistory_returnsExchangeRate() {
		String baseCurrency = "EUR";
		String targetCurrency = "USD";
		LocalDateTime start = LocalDateTime.now();
		LocalDateTime end = start.plusDays(1);
		BigDecimal rate = new BigDecimal("10.001");

		Mockito.when(currencyConversionFacade.getExchangeRates(baseCurrency, targetCurrency, start, end)).thenReturn(List.of(new ExchangeRateDto[]{new ExchangeRateDto(baseCurrency, targetCurrency, rate, LocalDateTime.now(), ExchangeRateSource.ECB)}));

		List<ExchangeRateDto> result = currencyConversionController.getHistoricalExchangeRates(baseCurrency, targetCurrency, start, end);

		assertThat(result).isNotEmpty();
		assertThat(result.getFirst().getBaseCurrency()).isEqualTo("EUR");
		assertThat(result.getFirst().getTargetCurrency()).isEqualTo("USD");
		assertThat(result.getFirst().getRate()).isEqualTo(rate);
	}
}
